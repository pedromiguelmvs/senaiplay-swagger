
window.onload = function() {
  // Build a system
  var url = window.location.search.match(/url=([^&]+)/);
  if (url && url.length > 1) {
    url = decodeURIComponent(url[1]);
  } else {
    url = window.location.origin;
  }
  var options = {
  "swaggerDoc": {
    "openapi": "3.0.0",
    "info": {
      "title": "Senaiplay Student",
      "description": "The Senaiplay Student API",
      "version": "1.0",
      "contact": {}
    },
    "tags": [
      {
        "name": "auth",
        "description": ""
      },
      {
        "name": "files",
        "description": ""
      },
      {
        "name": "students",
        "description": ""
      },
      {
        "name": "subscriptions",
        "description": ""
      },
      {
        "name": "units",
        "description": ""
      }
    ],
    "servers": [],
    "components": {
      "securitySchemes": {
        "bearer": {
          "scheme": "bearer",
          "bearerFormat": "JWT",
          "type": "http"
        }
      },
      "schemas": {
        "LoginDto": {
          "type": "object",
          "properties": {
            "cpfOrEmail": {
              "type": "string"
            },
            "password": {
              "type": "string"
            }
          },
          "required": [
            "cpfOrEmail",
            "password"
          ]
        },
        "units": {
          "type": "object",
          "properties": {
            "id": {
              "type": "number"
            },
            "regional": {
              "type": "number"
            },
            "code": {
              "type": "number"
            },
            "description": {
              "type": "string"
            }
          },
          "required": [
            "id",
            "regional",
            "code",
            "description"
          ]
        },
        "files": {
          "type": "object",
          "properties": {
            "id": {
              "type": "number"
            },
            "originalName": {
              "type": "string"
            },
            "hashname": {
              "type": "string"
            },
            "path": {
              "type": "string"
            },
            "type": {
              "type": "number"
            },
            "status": {
              "type": "number"
            },
            "sizeInBit": {
              "type": "number"
            },
            "isPrivate": {
              "type": "boolean"
            },
            "urlPublic": {
              "type": "string"
            }
          },
          "required": [
            "id",
            "originalName",
            "hashname",
            "path",
            "type",
            "status",
            "sizeInBit",
            "isPrivate",
            "urlPublic"
          ]
        },
        "course_categories": {
          "type": "object",
          "properties": {}
        },
        "alternatives": {
          "type": "object",
          "properties": {
            "id": {
              "type": "number"
            },
            "order": {
              "type": "number"
            },
            "description": {
              "type": "string"
            },
            "needJustification": {
              "type": "boolean"
            },
            "isCorrect": {
              "type": "boolean"
            },
            "feedback": {
              "type": "string"
            },
            "fileId": {
              "type": "number"
            },
            "file": {
              "$ref": "#/components/schemas/files"
            }
          },
          "required": [
            "id",
            "order",
            "description",
            "needJustification",
            "isCorrect",
            "feedback",
            "fileId",
            "file"
          ]
        },
        "questions": {
          "type": "object",
          "properties": {
            "id": {
              "type": "number"
            },
            "order": {
              "type": "number"
            },
            "description": {
              "type": "string"
            },
            "answerType": {
              "type": "object"
            },
            "type": {
              "type": "object"
            },
            "alternativeOrder": {
              "type": "number"
            },
            "responseIsRequired": {
              "type": "boolean"
            },
            "activityId": {
              "type": "number"
            },
            "alternatives": {
              "type": "array",
              "items": {
                "$ref": "#/components/schemas/alternatives"
              }
            }
          },
          "required": [
            "id",
            "order",
            "description",
            "answerType",
            "type",
            "alternativeOrder",
            "responseIsRequired",
            "activityId",
            "alternatives"
          ]
        },
        "activities": {
          "type": "object",
          "properties": {
            "id": {
              "type": "number"
            },
            "title": {
              "type": "string"
            },
            "questionsOrder": {
              "type": "number"
            },
            "questionsExhibition": {
              "type": "number"
            },
            "feedbackMoment": {
              "type": "number"
            },
            "isFinished": {
              "type": "boolean"
            },
            "questions": {
              "type": "array",
              "items": {
                "$ref": "#/components/schemas/questions"
              }
            },
            "avaliationType": {
              "type": "number"
            },
            "areaId": {
              "type": "number"
            },
            "area": {
              "type": "object"
            },
            "tags": {
              "type": "string"
            }
          },
          "required": [
            "id",
            "title",
            "questionsOrder",
            "questionsExhibition",
            "feedbackMoment",
            "isFinished",
            "questions",
            "avaliationType",
            "areaId",
            "area",
            "tags"
          ]
        },
        "open_badges": {
          "type": "object",
          "properties": {}
        },
        "course_types": {
          "type": "object",
          "properties": {}
        },
        "subscription_tracking": {
          "type": "object",
          "properties": {
            "id": {
              "type": "number"
            },
            "subscriptionId": {
              "type": "number"
            },
            "createdAt": {
              "format": "date-time",
              "type": "string"
            },
            "type": {
              "type": "number"
            },
            "lessonVideoId": {
              "type": "number"
            },
            "lessonVideoActivityId": {
              "type": "number"
            },
            "lessonAttachmentActivityId": {
              "type": "number"
            },
            "lessonActivityId": {
              "type": "number"
            },
            "subscription": {
              "type": "object"
            }
          },
          "required": [
            "id",
            "subscriptionId",
            "createdAt",
            "type",
            "lessonVideoId",
            "lessonVideoActivityId",
            "lessonAttachmentActivityId",
            "lessonActivityId",
            "subscription"
          ]
        },
        "certificates": {
          "type": "object",
          "properties": {
            "id": {
              "type": "number"
            },
            "subscriptionId": {
              "type": "number"
            },
            "code": {
              "type": "string"
            },
            "createdAt": {
              "format": "date-time",
              "type": "string"
            },
            "subscription": {
              "type": "array",
              "items": {
                "type": "object"
              }
            }
          },
          "required": [
            "id",
            "subscriptionId",
            "code",
            "createdAt",
            "subscription"
          ]
        },
        "inactive_email_types": {
          "type": "object",
          "properties": {
            "id": {
              "type": "number"
            },
            "description": {
              "type": "string"
            },
            "createdAt": {
              "format": "date-time",
              "type": "string"
            },
            "updatedAt": {
              "format": "date-time",
              "type": "string"
            },
            "deletedAt": {
              "format": "date-time",
              "type": "string"
            },
            "subscriptions": {
              "type": "array",
              "items": {
                "type": "array"
              }
            }
          },
          "required": [
            "id",
            "description",
            "createdAt",
            "updatedAt",
            "deletedAt",
            "subscriptions"
          ]
        },
        "inactive_email_types_subscriptions": {
          "type": "object",
          "properties": {
            "subscriptionId": {
              "type": "number"
            },
            "inactiveEmailTypeId": {
              "type": "number"
            },
            "createdAt": {
              "format": "date-time",
              "type": "string"
            },
            "updatedAt": {
              "format": "date-time",
              "type": "string"
            },
            "deletedAt": {
              "format": "date-time",
              "type": "string"
            }
          },
          "required": [
            "subscriptionId",
            "inactiveEmailTypeId",
            "createdAt",
            "updatedAt",
            "deletedAt"
          ]
        },
        "subscriptions": {
          "type": "object",
          "properties": {
            "id": {
              "type": "number"
            },
            "studentId": {
              "type": "number"
            },
            "courseId": {
              "type": "number"
            },
            "createdAt": {
              "format": "date-time",
              "type": "string"
            },
            "lastAccessAt": {
              "format": "date-time",
              "type": "string"
            },
            "archivedAt": {
              "format": "date-time",
              "type": "string"
            },
            "videosQuantity": {
              "type": "number"
            },
            "activitiesQuantity": {
              "type": "number"
            },
            "rating": {
              "type": "number"
            },
            "hashname": {
              "type": "string"
            },
            "finishedAt": {
              "format": "date-time",
              "type": "string"
            },
            "belongsToSubscriptionId": {
              "type": "number"
            },
            "isMiniCourse": {
              "type": "boolean"
            },
            "isPaid": {
              "type": "boolean"
            },
            "progress": {
              "type": "object"
            },
            "course": {
              "type": "object"
            },
            "student": {
              "type": "object"
            },
            "subs": {
              "type": "array",
              "items": {
                "$ref": "#/components/schemas/subscriptions"
              }
            },
            "subscriptionsTracking": {
              "type": "array",
              "items": {
                "$ref": "#/components/schemas/subscription_tracking"
              }
            },
            "certified": {
              "$ref": "#/components/schemas/certificates"
            },
            "inactiveEmailTypes": {
              "type": "array",
              "items": {
                "$ref": "#/components/schemas/inactive_email_types"
              }
            },
            "inactiveEmailTypeSubscriptions": {
              "type": "array",
              "items": {
                "$ref": "#/components/schemas/inactive_email_types_subscriptions"
              }
            }
          },
          "required": [
            "id",
            "studentId",
            "courseId",
            "createdAt",
            "lastAccessAt",
            "archivedAt",
            "videosQuantity",
            "activitiesQuantity",
            "rating",
            "hashname",
            "finishedAt",
            "belongsToSubscriptionId",
            "isMiniCourse",
            "isPaid",
            "progress",
            "course",
            "student",
            "subs",
            "subscriptionsTracking",
            "certified",
            "inactiveEmailTypes",
            "inactiveEmailTypeSubscriptions"
          ]
        },
        "modules": {
          "type": "object",
          "properties": {}
        },
        "study_plan_types": {
          "type": "object",
          "properties": {
            "id": {
              "type": "number"
            },
            "description": {
              "type": "string"
            },
            "createdAt": {
              "format": "date-time",
              "type": "string"
            },
            "updatedAt": {
              "format": "date-time",
              "type": "string"
            },
            "deletedAt": {
              "format": "date-time",
              "type": "string"
            }
          },
          "required": [
            "id",
            "description",
            "createdAt",
            "updatedAt",
            "deletedAt"
          ]
        },
        "study_plan_colors": {
          "type": "object",
          "properties": {
            "id": {
              "type": "number"
            },
            "description": {
              "type": "string"
            },
            "code": {
              "type": "string"
            },
            "createdAt": {
              "format": "date-time",
              "type": "string"
            },
            "updatedAt": {
              "format": "date-time",
              "type": "string"
            },
            "deletedAt": {
              "format": "date-time",
              "type": "string"
            }
          },
          "required": [
            "id",
            "description",
            "code",
            "createdAt",
            "updatedAt",
            "deletedAt"
          ]
        },
        "study_plans": {
          "type": "object",
          "properties": {
            "id": {
              "type": "number"
            },
            "ownerId": {
              "type": "number"
            },
            "mainAreaId": {
              "type": "number"
            },
            "typeId": {
              "type": "number"
            },
            "colorId": {
              "type": "number"
            },
            "title": {
              "type": "string"
            },
            "description": {
              "type": "string"
            },
            "hashValue": {
              "type": "string"
            },
            "followersCounting": {
              "type": "number"
            },
            "createdAt": {
              "format": "date-time",
              "type": "string"
            },
            "updatedAt": {
              "format": "date-time",
              "type": "string"
            },
            "deletedAt": {
              "format": "date-time",
              "type": "string"
            },
            "courses": {
              "type": "array",
              "items": {
                "type": "array"
              }
            },
            "followers": {
              "type": "array",
              "items": {
                "type": "array"
              }
            },
            "owner": {
              "type": "object"
            },
            "mainArea": {
              "type": "object"
            },
            "type": {
              "$ref": "#/components/schemas/study_plan_types"
            },
            "totalCourses": {
              "type": "number"
            },
            "colorCode": {
              "type": "string"
            },
            "color": {
              "$ref": "#/components/schemas/study_plan_colors"
            },
            "isAlreadyFollower": {
              "type": "number"
            },
            "ownerName": {
              "type": "string"
            },
            "ownerPublicUrlPhoto": {
              "type": "string"
            },
            "areaDescription": {
              "type": "string"
            },
            "areaIconName": {
              "type": "string"
            },
            "badgePublicUrl": {
              "type": "string"
            },
            "isCompleted": {
              "type": "boolean"
            },
            "hasEmitedBadge": {
              "type": "boolean"
            }
          },
          "required": [
            "id",
            "ownerId",
            "mainAreaId",
            "typeId",
            "colorId",
            "title",
            "description",
            "hashValue",
            "followersCounting",
            "createdAt",
            "updatedAt",
            "deletedAt",
            "courses",
            "followers",
            "owner",
            "mainArea",
            "type",
            "totalCourses",
            "colorCode",
            "color",
            "isAlreadyFollower",
            "ownerName",
            "ownerPublicUrlPhoto",
            "areaDescription",
            "areaIconName",
            "badgePublicUrl",
            "isCompleted",
            "hasEmitedBadge"
          ]
        },
        "target_audiences": {
          "type": "object",
          "properties": {}
        },
        "course_target_audiences": {
          "type": "object",
          "properties": {}
        },
        "competences": {
          "type": "object",
          "properties": {}
        },
        "course_competences": {
          "type": "object",
          "properties": {}
        },
        "courses": {
          "type": "object",
          "properties": {
            "id": {
              "type": "number"
            },
            "name": {
              "type": "string"
            },
            "description": {
              "type": "string"
            },
            "categoryId": {
              "type": "number"
            },
            "syllabus": {
              "type": "string"
            },
            "workload": {
              "type": "number"
            },
            "coins": {
              "type": "number"
            },
            "published": {
              "type": "boolean"
            },
            "highlight": {
              "type": "boolean"
            },
            "fileId": {
              "type": "number"
            },
            "publishedAt": {
              "format": "date-time",
              "type": "string"
            },
            "typeId": {
              "type": "number"
            },
            "activityId": {
              "type": "number"
            },
            "tags": {
              "type": "string"
            },
            "affiliateCode": {
              "type": "number"
            },
            "branchCode": {
              "type": "number"
            },
            "marketPlaceUrl": {
              "type": "string"
            },
            "sku": {
              "type": "number"
            },
            "certificatePrice": {
              "type": "number"
            },
            "obsoleteAt": {
              "format": "date-time",
              "type": "string"
            },
            "rating": {
              "type": "number"
            },
            "courseCategory": {
              "$ref": "#/components/schemas/course_categories"
            },
            "file": {
              "$ref": "#/components/schemas/files"
            },
            "activity": {
              "$ref": "#/components/schemas/activities"
            },
            "areas": {
              "type": "array",
              "items": {
                "type": "array"
              }
            },
            "badge": {
              "$ref": "#/components/schemas/open_badges"
            },
            "type": {
              "$ref": "#/components/schemas/course_types"
            },
            "students": {
              "type": "array",
              "items": {
                "type": "array"
              }
            },
            "subscription": {
              "type": "array",
              "items": {
                "$ref": "#/components/schemas/subscriptions"
              }
            },
            "isFavorited": {
              "type": "object"
            },
            "progress": {
              "type": "object"
            },
            "microCourses": {
              "type": "array",
              "items": {
                "$ref": "#/components/schemas/courses"
              }
            },
            "microcourseCourses": {
              "type": "array",
              "items": {
                "$ref": "#/components/schemas/courses"
              }
            },
            "modules": {
              "type": "array",
              "items": {
                "$ref": "#/components/schemas/modules"
              }
            },
            "studyPlans": {
              "type": "array",
              "items": {
                "$ref": "#/components/schemas/study_plans"
              }
            },
            "targetAudiences": {
              "type": "array",
              "items": {
                "$ref": "#/components/schemas/target_audiences"
              }
            },
            "courseTargetAudiences": {
              "$ref": "#/components/schemas/course_target_audiences"
            },
            "competences": {
              "type": "array",
              "items": {
                "$ref": "#/components/schemas/competences"
              }
            },
            "courseCompetences": {
              "type": "array",
              "items": {
                "$ref": "#/components/schemas/course_competences"
              }
            }
          },
          "required": [
            "id",
            "name",
            "description",
            "categoryId",
            "syllabus",
            "workload",
            "coins",
            "published",
            "highlight",
            "fileId",
            "publishedAt",
            "typeId",
            "activityId",
            "tags",
            "affiliateCode",
            "branchCode",
            "marketPlaceUrl",
            "sku",
            "certificatePrice",
            "obsoleteAt",
            "rating",
            "courseCategory",
            "file",
            "activity",
            "areas",
            "badge",
            "type",
            "students",
            "subscription",
            "isFavorited",
            "progress",
            "microCourses",
            "microcourseCourses",
            "modules",
            "studyPlans",
            "targetAudiences",
            "courseTargetAudiences",
            "competences",
            "courseCompetences"
          ]
        },
        "email_authorizations": {
          "type": "object",
          "properties": {}
        },
        "students": {
          "type": "object",
          "properties": {
            "id": {
              "type": "number"
            },
            "name": {
              "type": "string"
            },
            "email": {
              "type": "string"
            },
            "cpf": {
              "type": "string"
            },
            "gender": {
              "type": "number"
            },
            "mobileNumber": {
              "type": "string"
            },
            "password": {
              "type": "string"
            },
            "regional": {
              "type": "number"
            },
            "unitId": {
              "type": "number"
            },
            "photoId": {
              "type": "number"
            },
            "acceptTermsOfUse": {
              "type": "boolean"
            },
            "passwordRecoveryToken": {
              "type": "string"
            },
            "passwordRecoveryExpiresDate": {
              "format": "date-time",
              "type": "string"
            },
            "lastAccessAt": {
              "format": "date-time",
              "type": "string"
            },
            "unit": {
              "$ref": "#/components/schemas/units"
            },
            "hashname": {
              "type": "string"
            },
            "photo": {
              "$ref": "#/components/schemas/files"
            },
            "courses": {
              "type": "array",
              "items": {
                "$ref": "#/components/schemas/courses"
              }
            },
            "student": {
              "type": "array",
              "items": {
                "$ref": "#/components/schemas/students"
              }
            },
            "subscription": {
              "$ref": "#/components/schemas/subscriptions"
            },
            "EmailUnauthorizations": {
              "type": "array",
              "items": {
                "$ref": "#/components/schemas/email_authorizations"
              }
            }
          },
          "required": [
            "id",
            "name",
            "email",
            "cpf",
            "gender",
            "mobileNumber",
            "password",
            "regional",
            "unitId",
            "photoId",
            "acceptTermsOfUse",
            "passwordRecoveryToken",
            "passwordRecoveryExpiresDate",
            "lastAccessAt",
            "unit",
            "hashname",
            "photo",
            "courses",
            "student",
            "subscription",
            "EmailUnauthorizations"
          ]
        },
        "CreateStudentDto": {
          "type": "object",
          "properties": {
            "name": {
              "type": "string"
            },
            "email": {
              "type": "string"
            },
            "emailConfirm": {
              "type": "string"
            },
            "cpf": {
              "type": "string"
            },
            "gender": {
              "type": "string",
              "enum": [
                "M",
                "F",
                "O"
              ]
            },
            "mobileNumber": {
              "type": "string"
            },
            "password": {
              "type": "string"
            },
            "passwordConfirm": {
              "type": "string",
              "description": "O campo [passwordConfirm] é utilizado apenas no update do estudante."
            },
            "regional": {
              "type": "number",
              "description": "Se o campo [hasLink] for true, esse campo se torna obrigatório."
            },
            "unitId": {
              "type": "number",
              "description": "Se o campo [hasLink] for true, esse campo se torna obrigatório."
            },
            "photoId": {
              "type": "number"
            },
            "hasLink": {
              "type": "boolean"
            },
            "newCourseAuthorization": {
              "type": "boolean"
            },
            "inactivityAuthorization": {
              "type": "boolean"
            },
            "welcomeToCourseAuthorization": {
              "type": "boolean"
            },
            "acceptTermsOfUse": {
              "type": "boolean"
            }
          },
          "required": [
            "name",
            "email",
            "emailConfirm",
            "cpf",
            "gender",
            "mobileNumber",
            "password",
            "passwordConfirm",
            "regional",
            "unitId",
            "acceptTermsOfUse"
          ]
        },
        "StudentResponse": {
          "type": "object",
          "properties": {
            "student": {
              "$ref": "#/components/schemas/students"
            },
            "message": {
              "type": "string"
            }
          },
          "required": [
            "student",
            "message"
          ]
        },
        "UpdatePhotoDto": {
          "type": "object",
          "properties": {
            "photoId": {
              "type": "number"
            }
          }
        },
        "UpdateStudentDTO": {
          "type": "object",
          "properties": {
            "name": {
              "type": "string"
            },
            "email": {
              "type": "string"
            },
            "gender": {
              "type": "string",
              "enum": [
                "M",
                "F",
                "O"
              ]
            },
            "mobileNumber": {
              "type": "string"
            },
            "password": {
              "type": "string"
            },
            "passwordConfirm": {
              "type": "string",
              "description": "O campo [passwordConfirm] é utilizado apenas no update do estudante."
            },
            "regional": {
              "type": "number",
              "description": "Se o campo [hasLink] for true, esse campo se torna obrigatório."
            },
            "unitId": {
              "type": "number",
              "description": "Se o campo [hasLink] for true, esse campo se torna obrigatório."
            },
            "photoId": {
              "type": "number"
            },
            "newCourseAuthorization": {
              "type": "boolean"
            },
            "inactivityAuthorization": {
              "type": "boolean"
            },
            "welcomeToCourseAuthorization": {
              "type": "boolean"
            },
            "hasLink": {
              "type": "boolean"
            }
          }
        },
        "PasswordRecoveryDto": {
          "type": "object",
          "properties": {
            "password": {
              "type": "string"
            },
            "passwordConfirm": {
              "type": "string"
            }
          },
          "required": [
            "password",
            "passwordConfirm"
          ]
        },
        "DeleteAccountDto": {
          "type": "object",
          "properties": {
            "password": {
              "type": "string"
            }
          },
          "required": [
            "password"
          ]
        },
        "VerifyHashnameDto": {
          "type": "object",
          "properties": {
            "studentId": {
              "type": "number"
            },
            "hashname": {
              "type": "string"
            },
            "type": {
              "type": "string"
            }
          },
          "required": [
            "studentId",
            "hashname",
            "type"
          ]
        },
        "CreateUnauthorizationDto": {
          "type": "object",
          "properties": {
            "studentId": {
              "type": "number"
            },
            "hashname": {
              "type": "string"
            },
            "type": {
              "type": "string"
            }
          },
          "required": [
            "studentId",
            "hashname",
            "type"
          ]
        },
        "CreateSubscriptionDto": {
          "type": "object",
          "properties": {
            "courseId": {
              "type": "number"
            }
          },
          "required": [
            "courseId"
          ]
        },
        "SubscriptionControllerResponse": {
          "type": "object",
          "properties": {
            "subscription": {
              "$ref": "#/components/schemas/subscriptions"
            },
            "message": {
              "type": "string"
            }
          },
          "required": [
            "subscription",
            "message"
          ]
        },
        "VerifyIfHashnameExistsDto": {
          "type": "object",
          "properties": {
            "subscriptionId": {
              "type": "number"
            },
            "studentId": {
              "type": "number"
            },
            "hashname": {
              "type": "string"
            }
          },
          "required": [
            "subscriptionId",
            "studentId",
            "hashname"
          ]
        },
        "UpdateSubscriptionDto": {
          "type": "object",
          "properties": {
            "lastAccessAt": {
              "format": "date-time",
              "type": "string"
            },
            "archivedAt": {
              "format": "date-time",
              "type": "string"
            },
            "videosQuantity": {
              "type": "number"
            },
            "activitiesQuantity": {
              "type": "number"
            }
          }
        },
        "MessageResponse": {
          "type": "object",
          "properties": {
            "message": {
              "type": "string"
            }
          },
          "required": [
            "message"
          ]
        },
        "RatingCourseDto": {
          "type": "object",
          "properties": {
            "rating": {
              "type": "number"
            }
          },
          "required": [
            "rating"
          ]
        },
        "CreateWishlistDto": {
          "type": "object",
          "properties": {
            "courseId": {
              "type": "number"
            }
          },
          "required": [
            "courseId"
          ]
        },
        "QuestionOrder": {
          "type": "string",
          "enum": [
            "Sequenciadas",
            "Randômicas"
          ]
        },
        "QuestionsExhibition": {
          "type": "string",
          "enum": [
            "Lista",
            "Apenas uma por página"
          ]
        },
        "FeedbackMoment": {
          "type": "string",
          "enum": [
            "A cada resposta",
            "No final da avaliação"
          ]
        },
        "QuestionType": {
          "type": "string",
          "enum": [
            "Apenas uma resposta correta",
            "Mais de uma resposta correta"
          ]
        },
        "AlternativeOrder": {
          "type": "string",
          "enum": [
            "Sequenciadas",
            "Randômicas"
          ]
        },
        "CreateQuestionsDto": {
          "type": "object",
          "properties": {
            "activityId": {
              "type": "number"
            },
            "order": {
              "type": "number"
            },
            "description": {
              "type": "string"
            },
            "answerType": {
              "$ref": "#/components/schemas/QuestionType"
            },
            "alternativeOrder": {
              "$ref": "#/components/schemas/AlternativeOrder"
            },
            "responseIsRequired": {
              "type": "boolean"
            }
          },
          "required": [
            "activityId",
            "order",
            "description",
            "answerType",
            "alternativeOrder",
            "responseIsRequired"
          ]
        },
        "AvaliationType": {
          "type": "string",
          "enum": [
            "Avaliação somativa",
            "Avaliação formativa"
          ]
        },
        "CreateActivityDto": {
          "type": "object",
          "properties": {
            "title": {
              "type": "string"
            },
            "questionsOrder": {
              "$ref": "#/components/schemas/QuestionOrder"
            },
            "questionsExhibition": {
              "$ref": "#/components/schemas/QuestionsExhibition"
            },
            "feedbackMoment": {
              "$ref": "#/components/schemas/FeedbackMoment"
            },
            "questions": {
              "$ref": "#/components/schemas/CreateQuestionsDto"
            },
            "avaliationType": {
              "$ref": "#/components/schemas/AvaliationType"
            },
            "areaId": {
              "type": "number"
            },
            "tags": {
              "type": "array",
              "items": {
                "type": "string"
              }
            }
          },
          "required": [
            "title",
            "questionsOrder",
            "questionsExhibition",
            "feedbackMoment",
            "questions",
            "avaliationType",
            "areaId",
            "tags"
          ]
        },
        "CreateAnswerDto": {
          "type": "object",
          "properties": {
            "questions": {
              "type": "array",
              "items": {
                "type": "string"
              }
            },
            "subscriptionId": {
              "type": "number"
            },
            "lessonVideoActivityId": {
              "type": "number"
            },
            "lessonAttachmentActivityId": {
              "type": "number"
            },
            "lessonActivityId": {
              "type": "number"
            },
            "activityId": {
              "type": "number"
            }
          },
          "required": [
            "questions",
            "subscriptionId",
            "activityId"
          ]
        },
        "CreateAnswerResponse": {
          "type": "object",
          "properties": {
            "question": {
              "type": "object"
            },
            "message": {
              "type": "string"
            },
            "finished": {
              "type": "boolean"
            }
          },
          "required": [
            "question",
            "message",
            "finished"
          ]
        },
        "CreateAlternativeDto": {
          "type": "object",
          "properties": {
            "questionId": {
              "type": "number"
            },
            "order": {
              "type": "number"
            },
            "description": {
              "type": "string"
            },
            "needJustification": {
              "type": "boolean"
            },
            "isCorrect": {
              "type": "boolean"
            },
            "feedback": {
              "type": "string"
            }
          },
          "required": [
            "questionId",
            "order",
            "description",
            "needJustification",
            "isCorrect"
          ]
        },
        "FileUploadDto": {
          "type": "object",
          "properties": {
            "files": {
              "type": "string",
              "format": "binary"
            }
          },
          "required": [
            "files"
          ]
        },
        "CreateInterestAreaDto": {
          "type": "object",
          "properties": {
            "areaIds": {
              "type": "array",
              "items": {
                "type": "string"
              }
            }
          },
          "required": [
            "areaIds"
          ]
        },
        "interest_areas": {
          "type": "object",
          "properties": {
            "id": {
              "type": "number"
            },
            "studentId": {
              "type": "number"
            },
            "areaId": {
              "type": "number"
            },
            "student": {
              "$ref": "#/components/schemas/students"
            },
            "area": {
              "type": "object"
            }
          },
          "required": [
            "id",
            "studentId",
            "areaId",
            "student",
            "area"
          ]
        },
        "CreateSubscriptionTrackingDto": {
          "type": "object",
          "properties": {
            "subscriptionId": {
              "type": "number"
            },
            "type": {
              "type": "string",
              "enum": [
                "activity",
                "video",
                "audio"
              ]
            },
            "lessonVideoId": {
              "type": "number"
            },
            "lessonVideoActivityId": {
              "type": "number"
            },
            "lessonAttachmentActivityId": {
              "type": "number"
            },
            "lessonActivityId": {
              "type": "number"
            },
            "originCourseId": {
              "type": "number"
            }
          },
          "required": [
            "subscriptionId",
            "type",
            "lessonVideoId"
          ]
        },
        "CreateSubscriptionTrackingResponse": {
          "type": "object",
          "properties": {
            "message": {
              "type": "string"
            },
            "subscriptionTracking": {
              "$ref": "#/components/schemas/subscription_tracking"
            },
            "progress": {
              "type": "number"
            }
          },
          "required": [
            "message",
            "subscriptionTracking",
            "progress"
          ]
        },
        "GetSubscriptionTrackingResponse": {
          "type": "object",
          "properties": {
            "completedActivities": {
              "$ref": "#/components/schemas/subscription_tracking"
            },
            "completedVideos": {
              "$ref": "#/components/schemas/subscription_tracking"
            }
          },
          "required": [
            "completedActivities",
            "completedVideos"
          ]
        },
        "notified_users": {
          "type": "object",
          "properties": {
            "id": {
              "type": "number"
            },
            "notificationMetadataId": {
              "type": "number"
            },
            "userId": {
              "type": "number"
            },
            "studentId": {
              "type": "number"
            },
            "viewed": {
              "type": "boolean"
            }
          },
          "required": [
            "id",
            "notificationMetadataId",
            "userId",
            "studentId",
            "viewed"
          ]
        },
        "NotificationResponse": {
          "type": "object",
          "properties": {
            "totalNotifications": {
              "type": "number"
            },
            "notifications": {
              "type": "array",
              "items": {
                "$ref": "#/components/schemas/notified_users"
              }
            }
          },
          "required": [
            "totalNotifications",
            "notifications"
          ]
        },
        "UpdateNotificationDto": {
          "type": "object",
          "properties": {
            "ids": {
              "type": "array",
              "items": {
                "type": "number"
              }
            }
          },
          "required": [
            "ids"
          ]
        },
        "CreateCertifiedDto": {
          "type": "object",
          "properties": {
            "subscriptionId": {
              "type": "number"
            },
            "courseId": {
              "type": "number"
            }
          },
          "required": [
            "subscriptionId"
          ]
        },
        "SendEmailDto": {
          "type": "object",
          "properties": {
            "name": {
              "type": "string"
            },
            "recaptchaValue": {
              "type": "string"
            },
            "email": {
              "type": "string"
            },
            "subject": {
              "type": "string"
            },
            "message": {
              "type": "string"
            },
            "lessonId": {
              "type": "number"
            },
            "courseId": {
              "type": "number"
            },
            "studentId": {
              "type": "number"
            }
          },
          "required": [
            "name",
            "recaptchaValue",
            "email",
            "subject",
            "message",
            "lessonId",
            "courseId",
            "studentId"
          ]
        },
        "LessonDto": {
          "type": "object",
          "properties": {
            "moduleId": {
              "type": "number"
            },
            "title": {
              "type": "string"
            },
            "description": {
              "type": "string"
            },
            "order": {
              "type": "number"
            }
          },
          "required": [
            "moduleId",
            "title",
            "description",
            "order"
          ]
        },
        "lessons": {
          "type": "object",
          "properties": {}
        },
        "LessonResponse": {
          "type": "object",
          "properties": {
            "lesson": {
              "$ref": "#/components/schemas/lessons"
            },
            "message": {
              "type": "string"
            }
          },
          "required": [
            "lesson",
            "message"
          ]
        },
        "LessonVideoDto": {
          "type": "object",
          "properties": {
            "lessonId": {
              "type": "number"
            },
            "videoId": {
              "type": "number"
            }
          },
          "required": [
            "lessonId",
            "videoId"
          ]
        },
        "platform_videos": {
          "type": "object",
          "properties": {}
        },
        "lesson_video_activities": {
          "type": "object",
          "properties": {
            "id": {
              "type": "number"
            },
            "lessonVideoId": {
              "type": "number"
            },
            "activityId": {
              "type": "number"
            },
            "startTime": {
              "type": "string"
            },
            "activity": {
              "$ref": "#/components/schemas/activities"
            },
            "subscriptionTracking": {
              "type": "object"
            }
          },
          "required": [
            "id",
            "lessonVideoId",
            "activityId",
            "startTime",
            "activity",
            "subscriptionTracking"
          ]
        },
        "lesson_videos": {
          "type": "object",
          "properties": {
            "id": {
              "type": "number"
            },
            "lessonId": {
              "type": "number"
            },
            "videoId": {
              "type": "number"
            },
            "lesson": {
              "$ref": "#/components/schemas/lessons"
            },
            "video": {
              "$ref": "#/components/schemas/platform_videos"
            },
            "videoActivity": {
              "type": "array",
              "items": {
                "$ref": "#/components/schemas/lesson_video_activities"
              }
            },
            "subscriptionTracking": {
              "type": "object"
            }
          },
          "required": [
            "id",
            "lessonId",
            "videoId",
            "lesson",
            "video",
            "videoActivity",
            "subscriptionTracking"
          ]
        },
        "LessonVideoResponse": {
          "type": "object",
          "properties": {
            "lessonVideo": {
              "$ref": "#/components/schemas/lesson_videos"
            },
            "message": {
              "type": "string"
            }
          },
          "required": [
            "lessonVideo",
            "message"
          ]
        },
        "LessonVideoActivityDto": {
          "type": "object",
          "properties": {
            "lessonVideoId": {
              "type": "number"
            },
            "activityId": {
              "type": "number"
            },
            "startTime": {
              "type": "string"
            }
          },
          "required": [
            "lessonVideoId",
            "activityId",
            "startTime"
          ]
        },
        "LessonVideoAcitivtyResponse": {
          "type": "object",
          "properties": {
            "lessonVideoActivity": {
              "$ref": "#/components/schemas/lesson_video_activities"
            },
            "message": {
              "type": "string"
            }
          },
          "required": [
            "lessonVideoActivity",
            "message"
          ]
        },
        "LessonActivityDto": {
          "type": "object",
          "properties": {
            "lessonId": {
              "type": "number"
            },
            "activityId": {
              "type": "number"
            }
          },
          "required": [
            "lessonId",
            "activityId"
          ]
        },
        "LessonAcitivtyResponse": {
          "type": "object",
          "properties": {
            "activity": {
              "$ref": "#/components/schemas/activities"
            },
            "message": {
              "type": "string"
            }
          },
          "required": [
            "activity",
            "message"
          ]
        },
        "LessonAttachmentDto": {
          "type": "object",
          "properties": {
            "lessonId": {
              "type": "number"
            },
            "description": {
              "type": "string"
            },
            "isLink": {
              "type": "boolean"
            },
            "url": {
              "type": "string"
            },
            "fileId": {
              "type": "number"
            }
          },
          "required": [
            "lessonId",
            "isLink"
          ]
        },
        "lesson_attachments": {
          "type": "object",
          "properties": {}
        },
        "LessonAttachmentResponse": {
          "type": "object",
          "properties": {
            "lessonAttachment": {
              "$ref": "#/components/schemas/lesson_attachments"
            },
            "message": {
              "type": "string"
            }
          },
          "required": [
            "lessonAttachment",
            "message"
          ]
        },
        "interactions": {
          "type": "object",
          "properties": {
            "id": {
              "type": "number"
            },
            "studentId": {
              "type": "number"
            },
            "lessonId": {
              "type": "number"
            },
            "typeInteraction": {
              "type": "number"
            }
          },
          "required": [
            "id",
            "studentId",
            "lessonId",
            "typeInteraction"
          ]
        },
        "StudyPlanDTO": {
          "type": "object",
          "properties": {
            "typeId": {
              "type": "number"
            },
            "mainAreaId": {
              "type": "number"
            },
            "colorId": {
              "type": "number"
            },
            "title": {
              "type": "string"
            },
            "description": {
              "type": "string"
            },
            "badgeBase64": {
              "type": "string"
            }
          },
          "required": [
            "typeId",
            "mainAreaId",
            "title",
            "description",
            "badgeBase64"
          ]
        },
        "StudyPlanCourseDTO": {
          "type": "object",
          "properties": {
            "studyPlanId": {
              "type": "number"
            },
            "courseId": {
              "type": "number"
            }
          },
          "required": [
            "studyPlanId",
            "courseId"
          ]
        },
        "EmitedBadgeResponse": {
          "type": "object",
          "properties": {}
        },
        "student_action_multipliers": {
          "type": "object",
          "properties": {
            "id": {
              "type": "number"
            },
            "studentActionId": {
              "type": "number"
            },
            "description": {
              "type": "string"
            },
            "keyRule": {
              "type": "string"
            },
            "operation": {
              "type": "string"
            },
            "value": {
              "type": "number"
            }
          },
          "required": [
            "id",
            "studentActionId",
            "description",
            "keyRule",
            "operation",
            "value"
          ]
        },
        "StudentActionMultiplierDto": {
          "type": "object",
          "properties": {
            "studentActionId": {
              "type": "number"
            },
            "description": {
              "type": "string"
            },
            "operation": {
              "type": "string"
            },
            "keyRule": {
              "type": "string"
            },
            "value": {
              "type": "number"
            }
          },
          "required": [
            "studentActionId",
            "description",
            "operation",
            "keyRule",
            "value"
          ]
        },
        "student_points": {
          "type": "object",
          "properties": {
            "id": {
              "type": "number"
            },
            "studentId": {
              "type": "number"
            },
            "actionTypeId": {
              "type": "number"
            },
            "xpReference": {
              "type": "number"
            },
            "xpEarned": {
              "type": "number"
            },
            "createdAt": {
              "format": "date-time",
              "type": "string"
            }
          },
          "required": [
            "id",
            "studentId",
            "actionTypeId",
            "xpReference",
            "xpEarned",
            "createdAt"
          ]
        },
        "StudentPointDto": {
          "type": "object",
          "properties": {
            "studentId": {
              "type": "number"
            },
            "actionTypeId": {
              "type": "number"
            },
            "xpReference": {
              "type": "number"
            },
            "xpEarned": {
              "type": "number"
            }
          },
          "required": [
            "studentId",
            "actionTypeId",
            "xpReference",
            "xpEarned"
          ]
        },
        "roles": {
          "type": "object",
          "properties": {}
        },
        "columnists_info": {
          "type": "object",
          "properties": {}
        },
        "users": {
          "type": "object",
          "properties": {
            "id": {
              "type": "number"
            },
            "name": {
              "type": "string"
            },
            "email": {
              "type": "string"
            },
            "photoFileId": {
              "type": "number"
            },
            "regional": {
              "type": "number"
            },
            "photo": {
              "$ref": "#/components/schemas/files"
            },
            "roles": {
              "type": "array",
              "items": {
                "$ref": "#/components/schemas/roles"
              }
            },
            "columnistInfo": {
              "$ref": "#/components/schemas/columnists_info"
            }
          },
          "required": [
            "id",
            "name",
            "email",
            "photoFileId",
            "regional",
            "photo",
            "roles",
            "columnistInfo"
          ]
        },
        "areas": {
          "type": "object",
          "properties": {
            "id": {
              "type": "number"
            },
            "description": {
              "type": "string"
            },
            "iconName": {
              "type": "string"
            },
            "text": {
              "type": "string"
            },
            "totalCourses": {
              "type": "object"
            },
            "normalizedDescription": {
              "type": "string"
            },
            "interestAreas": {
              "type": "array",
              "items": {
                "$ref": "#/components/schemas/interest_areas"
              }
            }
          },
          "required": [
            "id",
            "description",
            "iconName",
            "text",
            "totalCourses",
            "normalizedDescription",
            "interestAreas"
          ]
        },
        "platform_audios": {
          "type": "object",
          "properties": {}
        },
        "news": {
          "type": "object",
          "properties": {
            "id": {
              "type": "number"
            },
            "title": {
              "type": "string"
            },
            "text": {
              "type": "string"
            },
            "postType": {
              "type": "number"
            },
            "publishedAt": {
              "format": "date-time",
              "type": "string"
            },
            "published": {
              "type": "boolean"
            },
            "userId": {
              "type": "number"
            },
            "user": {
              "$ref": "#/components/schemas/users"
            },
            "area": {
              "$ref": "#/components/schemas/areas"
            },
            "photo": {
              "$ref": "#/components/schemas/files"
            },
            "addButton": {
              "type": "boolean"
            },
            "buttonText": {
              "type": "string"
            },
            "buttonUrl": {
              "type": "string"
            },
            "videoTitle": {
              "type": "string"
            },
            "videoUrl": {
              "type": "string"
            },
            "videoDescription": {
              "type": "string"
            },
            "podcastDescription": {
              "type": "string"
            },
            "podcastUrl": {
              "type": "string"
            },
            "viewsCount": {
              "type": "number"
            },
            "approvedByUserId": {
              "type": "number"
            },
            "audioId": {
              "type": "number"
            },
            "audio": {
              "$ref": "#/components/schemas/platform_audios"
            }
          },
          "required": [
            "id",
            "title",
            "text",
            "postType",
            "publishedAt",
            "published",
            "userId",
            "user",
            "area",
            "photo",
            "addButton",
            "buttonText",
            "buttonUrl",
            "videoTitle",
            "videoUrl",
            "videoDescription",
            "podcastDescription",
            "podcastUrl",
            "viewsCount",
            "approvedByUserId",
            "audioId",
            "audio"
          ]
        },
        "surveys": {
          "type": "object",
          "properties": {
            "id": {
              "type": "number"
            },
            "title": {
              "type": "string"
            },
            "isFinished": {
              "type": "boolean"
            },
            "isActive": {
              "type": "boolean"
            },
            "questionsExhibition": {
              "type": "number"
            },
            "updatedAt": {
              "format": "date-time",
              "type": "string"
            },
            "userId": {
              "type": "number"
            },
            "questions": {
              "type": "array",
              "items": {
                "type": "array"
              }
            }
          },
          "required": [
            "id",
            "title",
            "isFinished",
            "isActive",
            "questionsExhibition",
            "updatedAt",
            "userId",
            "questions"
          ]
        },
        "StudentStatusResponse": {
          "type": "object",
          "properties": {
            "answeredActiveSurvey": {
              "type": "boolean"
            }
          },
          "required": [
            "answeredActiveSurvey"
          ]
        },
        "SurveyAnswerDto": {
          "type": "object",
          "properties": {
            "subscriptionId": {
              "type": "number"
            },
            "surveyQuestionId": {
              "type": "number"
            },
            "alternative": {
              "type": "object"
            }
          },
          "required": [
            "subscriptionId",
            "surveyQuestionId",
            "alternative"
          ]
        },
        "survey_answer": {
          "type": "object",
          "properties": {
            "id": {
              "type": "number"
            },
            "subscriptionId": {
              "type": "number"
            },
            "surveyQuestionId": {
              "type": "number"
            },
            "surveyQuestion": {
              "type": "object"
            },
            "alternative": {
              "type": "number"
            }
          },
          "required": [
            "id",
            "subscriptionId",
            "surveyQuestionId",
            "surveyQuestion",
            "alternative"
          ]
        },
        "curriculum_work_experiences": {
          "type": "object",
          "properties": {
            "id": {
              "type": "number"
            },
            "curriculumId": {
              "type": "number"
            },
            "title": {
              "type": "string"
            },
            "jobType": {
              "type": "number"
            },
            "companyName": {
              "type": "string"
            },
            "location": {
              "type": "string"
            },
            "startDate": {
              "format": "date-time",
              "type": "string"
            },
            "endDate": {
              "format": "date-time",
              "type": "string"
            }
          },
          "required": [
            "id",
            "curriculumId",
            "title",
            "jobType",
            "companyName",
            "location",
            "startDate",
            "endDate"
          ]
        },
        "CurriculumWorkExperienceDto": {
          "type": "object",
          "properties": {
            "curriculumId": {
              "type": "number"
            },
            "title": {
              "type": "string"
            },
            "jobType": {
              "type": "object"
            },
            "companyName": {
              "type": "string"
            },
            "location": {
              "type": "string"
            },
            "startDate": {
              "format": "date-time",
              "type": "string"
            },
            "endDate": {
              "format": "date-time",
              "type": "string"
            }
          },
          "required": [
            "curriculumId",
            "title",
            "jobType",
            "companyName",
            "startDate"
          ]
        },
        "UpdateCurriculumWorkExperienceDto": {
          "type": "object",
          "properties": {
            "title": {
              "type": "string"
            },
            "jobType": {
              "type": "object"
            },
            "companyName": {
              "type": "string"
            },
            "location": {
              "type": "string"
            },
            "startDate": {
              "format": "date-time",
              "type": "string"
            },
            "endDate": {
              "format": "date-time",
              "type": "string"
            }
          }
        },
        "curriculum_educational_background": {
          "type": "object",
          "properties": {
            "id": {
              "type": "number"
            },
            "curriculumId": {
              "type": "number"
            },
            "curriculum": {
              "type": "object"
            },
            "degreeType": {
              "type": "string"
            },
            "educationalInstitution": {
              "type": "string"
            },
            "studyField": {
              "type": "string"
            },
            "startDate": {
              "format": "date-time",
              "type": "string"
            },
            "endDate": {
              "format": "date-time",
              "type": "string"
            }
          },
          "required": [
            "id",
            "curriculumId",
            "curriculum",
            "degreeType",
            "educationalInstitution",
            "studyField",
            "startDate",
            "endDate"
          ]
        },
        "CurriculumEducationalBackgroundDto": {
          "type": "object",
          "properties": {
            "curriculumId": {
              "type": "number"
            },
            "degreeType": {
              "type": "string"
            },
            "educationalInstitution": {
              "type": "string"
            },
            "studyField": {
              "type": "string"
            },
            "startDate": {
              "format": "date-time",
              "type": "string"
            },
            "endDate": {
              "format": "date-time",
              "type": "string"
            }
          },
          "required": [
            "curriculumId",
            "educationalInstitution"
          ]
        },
        "UpdateCurriculumEducationalBackgroundDto": {
          "type": "object",
          "properties": {
            "degreeType": {
              "type": "string"
            },
            "educationalInstitution": {
              "type": "string"
            },
            "studyField": {
              "type": "string"
            },
            "startDate": {
              "format": "date-time",
              "type": "string"
            },
            "endDate": {
              "format": "date-time",
              "type": "string"
            }
          }
        },
        "curriculum_external_certificates": {
          "type": "object",
          "properties": {
            "id": {
              "type": "number"
            },
            "curriculumId": {
              "type": "number"
            },
            "name": {
              "type": "string"
            },
            "issuingOrganization": {
              "type": "string"
            },
            "credentialCode": {
              "type": "string"
            },
            "credentialUrl": {
              "type": "string"
            },
            "issuingDate": {
              "format": "date-time",
              "type": "string"
            },
            "expires": {
              "type": "boolean"
            },
            "expireDate": {
              "format": "date-time",
              "type": "string"
            }
          },
          "required": [
            "id",
            "curriculumId",
            "name",
            "issuingOrganization",
            "credentialCode",
            "credentialUrl",
            "issuingDate",
            "expires",
            "expireDate"
          ]
        },
        "CurriculumExternalCertificateDto": {
          "type": "object",
          "properties": {
            "curriculumId": {
              "type": "number"
            },
            "name": {
              "type": "string"
            },
            "issuingOrganization": {
              "type": "string"
            },
            "credentialCode": {
              "type": "string"
            },
            "credentialUrl": {
              "type": "string"
            },
            "issuingDate": {
              "format": "date-time",
              "type": "string"
            },
            "expires": {
              "type": "boolean"
            },
            "expireDate": {
              "format": "date-time",
              "type": "string"
            }
          },
          "required": [
            "curriculumId",
            "name",
            "issuingOrganization",
            "expires"
          ]
        },
        "UpdateCurriculumExternalCertificateDto": {
          "type": "object",
          "properties": {
            "name": {
              "type": "string"
            },
            "issuingOrganization": {
              "type": "string"
            },
            "credentialCode": {
              "type": "string"
            },
            "credentialUrl": {
              "type": "string"
            },
            "issuingDate": {
              "format": "date-time",
              "type": "string"
            },
            "expires": {
              "type": "boolean"
            },
            "expireDate": {
              "format": "date-time",
              "type": "string"
            }
          }
        },
        "curriculum_languages": {
          "type": "object",
          "properties": {
            "id": {
              "type": "number"
            },
            "curriculumId": {
              "type": "number"
            },
            "language": {
              "type": "string"
            },
            "proficiency": {
              "type": "string"
            },
            "curriculum": {
              "type": "object"
            }
          },
          "required": [
            "id",
            "curriculumId",
            "language",
            "proficiency",
            "curriculum"
          ]
        },
        "curriculum_social_medias": {
          "type": "object",
          "properties": {
            "id": {
              "type": "number"
            },
            "curriculumId": {
              "type": "number"
            },
            "socialMedia": {
              "type": "string"
            },
            "url": {
              "type": "string"
            },
            "curriculum": {
              "type": "object"
            }
          },
          "required": [
            "id",
            "curriculumId",
            "socialMedia",
            "url",
            "curriculum"
          ]
        },
        "curriculum_public_informations": {
          "type": "object",
          "properties": {
            "id": {
              "type": "number"
            },
            "curriculumId": {
              "type": "number"
            },
            "curriculumField": {
              "type": "string"
            },
            "status": {
              "type": "boolean"
            },
            "curriculum": {
              "type": "object"
            }
          },
          "required": [
            "id",
            "curriculumId",
            "curriculumField",
            "status",
            "curriculum"
          ]
        },
        "curriculum": {
          "type": "object",
          "properties": {
            "id": {
              "type": "number"
            },
            "studentId": {
              "type": "number"
            },
            "title": {
              "type": "string"
            },
            "location": {
              "type": "string"
            },
            "about": {
              "type": "string"
            },
            "student": {
              "$ref": "#/components/schemas/students"
            },
            "curriculumWorkExperiences": {
              "type": "array",
              "items": {
                "type": "array"
              }
            },
            "curriculumEducationalBackgrounds": {
              "type": "array",
              "items": {
                "$ref": "#/components/schemas/curriculum_educational_background"
              }
            },
            "curriculumExternalCertificates": {
              "type": "array",
              "items": {
                "$ref": "#/components/schemas/curriculum_external_certificates"
              }
            },
            "curriculumLanguages": {
              "type": "array",
              "items": {
                "$ref": "#/components/schemas/curriculum_languages"
              }
            },
            "curriculumSocialMedias": {
              "type": "array",
              "items": {
                "$ref": "#/components/schemas/curriculum_social_medias"
              }
            },
            "curriculumPublicInformation": {
              "type": "array",
              "items": {
                "$ref": "#/components/schemas/curriculum_public_informations"
              }
            }
          },
          "required": [
            "id",
            "studentId",
            "title",
            "location",
            "about",
            "student",
            "curriculumWorkExperiences",
            "curriculumEducationalBackgrounds",
            "curriculumExternalCertificates",
            "curriculumLanguages",
            "curriculumSocialMedias",
            "curriculumPublicInformation"
          ]
        },
        "UpdateCurriculumDto": {
          "type": "object",
          "properties": {
            "title": {
              "type": "string"
            },
            "location": {
              "type": "string"
            },
            "about": {
              "type": "string"
            },
            "socialMedias": {
              "type": "array",
              "items": {
                "type": "string"
              }
            },
            "publicInfos": {
              "type": "array",
              "items": {
                "type": "string"
              }
            }
          }
        },
        "GeneratePdfCurriculumDto": {
          "type": "object",
          "properties": {
            "profile": {
              "type": "object"
            },
            "about": {
              "type": "string"
            },
            "educationalBackgroundSection": {
              "type": "array",
              "items": {
                "type": "string"
              }
            },
            "workExperiencesSection": {
              "type": "array",
              "items": {
                "type": "string"
              }
            },
            "playCoursesSection": {
              "type": "array",
              "items": {
                "type": "string"
              }
            },
            "externalCertificatesSection": {
              "type": "array",
              "items": {
                "type": "string"
              }
            },
            "languagesSection": {
              "type": "array",
              "items": {
                "type": "string"
              }
            }
          },
          "required": [
            "profile"
          ]
        },
        "CurriculumLanguageDto": {
          "type": "object",
          "properties": {
            "curriculumId": {
              "type": "number"
            },
            "language": {
              "type": "string"
            },
            "proficiency": {
              "type": "string"
            }
          },
          "required": [
            "curriculumId",
            "language"
          ]
        },
        "UpdateCurriculumLanguageDto": {
          "type": "object",
          "properties": {
            "language": {
              "type": "string"
            },
            "proficiency": {
              "type": "string"
            }
          }
        },
        "CurriculumSocialMediaDto": {
          "type": "object",
          "properties": {
            "curriculumId": {
              "type": "number"
            },
            "socialMedia": {
              "type": "string"
            },
            "url": {
              "type": "string"
            }
          },
          "required": [
            "curriculumId",
            "socialMedia",
            "url"
          ]
        },
        "UpdateCurriculumSocialMediaDto": {
          "type": "object",
          "properties": {
            "socialMedia": {
              "type": "string"
            },
            "url": {
              "type": "string"
            }
          }
        },
        "student-activity-veracities": {
          "type": "object",
          "properties": {
            "id": {
              "type": "number"
            },
            "subscriptionId": {
              "type": "number"
            },
            "studentIp": {
              "type": "string"
            },
            "platform": {
              "type": "string"
            }
          },
          "required": [
            "id",
            "subscriptionId",
            "studentIp",
            "platform"
          ]
        },
        "VeracityDto": {
          "type": "object",
          "properties": {
            "subscriptionId": {
              "type": "number"
            },
            "studentIp": {
              "type": "string"
            },
            "platform": {
              "type": "string"
            }
          },
          "required": [
            "subscriptionId"
          ]
        },
        "banners": {
          "type": "object",
          "properties": {
            "id": {
              "type": "number"
            },
            "fileId": {
              "type": "number"
            },
            "photo": {
              "$ref": "#/components/schemas/files"
            },
            "url": {
              "type": "string"
            },
            "active": {
              "type": "boolean"
            }
          },
          "required": [
            "id",
            "fileId",
            "photo",
            "url",
            "active"
          ]
        },
        "Unit": {
          "type": "object",
          "properties": {
            "id": {
              "type": "number"
            },
            "regional": {
              "type": "number"
            },
            "code": {
              "type": "number"
            },
            "description": {
              "type": "string"
            }
          },
          "required": [
            "id",
            "regional",
            "code",
            "description"
          ]
        },
        "User": {
          "type": "object",
          "properties": {}
        },
        "Student": {
          "type": "object",
          "properties": {}
        },
        "student_invite_courses": {
          "type": "object",
          "properties": {}
        },
        "student_invites": {
          "type": "object",
          "properties": {
            "id": {
              "type": "number"
            },
            "name": {
              "type": "string"
            },
            "cpf": {
              "type": "string"
            },
            "gender": {
              "type": "number"
            },
            "mobileNumber": {
              "type": "string"
            },
            "regional": {
              "type": "number"
            },
            "unitId": {
              "type": "number"
            },
            "email": {
              "type": "string"
            },
            "token": {
              "type": "string"
            },
            "userId": {
              "type": "number"
            },
            "studentId": {
              "type": "number"
            },
            "sendedAt": {
              "format": "date-time",
              "type": "string"
            },
            "usedAt": {
              "format": "date-time",
              "type": "string"
            },
            "unit": {
              "$ref": "#/components/schemas/Unit"
            },
            "user": {
              "$ref": "#/components/schemas/User"
            },
            "student": {
              "$ref": "#/components/schemas/Student"
            },
            "studentInviteCourses": {
              "type": "array",
              "items": {
                "$ref": "#/components/schemas/student_invite_courses"
              }
            },
            "linkStudentInvite": {
              "type": "string"
            }
          },
          "required": [
            "id",
            "name",
            "cpf",
            "gender",
            "mobileNumber",
            "regional",
            "unitId",
            "email",
            "token",
            "userId",
            "studentId",
            "sendedAt",
            "usedAt",
            "unit",
            "user",
            "student",
            "studentInviteCourses",
            "linkStudentInvite"
          ]
        },
        "InvitedStudentDto": {
          "type": "object",
          "properties": {
            "name": {
              "type": "string"
            },
            "email": {
              "type": "string"
            },
            "emailConfirm": {
              "type": "string"
            },
            "cpf": {
              "type": "string"
            },
            "gender": {
              "type": "string",
              "enum": [
                "M",
                "F",
                "O"
              ]
            },
            "mobileNumber": {
              "type": "string"
            },
            "password": {
              "type": "string"
            },
            "passwordConfirm": {
              "type": "string",
              "description": "O campo [passwordConfirm] é utilizado apenas no update do estudante."
            },
            "regional": {
              "type": "number",
              "description": "Se o campo [hasLink] for true, esse campo se torna obrigatório."
            },
            "unitId": {
              "type": "number",
              "description": "Se o campo [hasLink] for true, esse campo se torna obrigatório."
            },
            "photoId": {
              "type": "number"
            },
            "hasLink": {
              "type": "boolean"
            },
            "newCourseAuthorization": {
              "type": "boolean"
            },
            "inactivityAuthorization": {
              "type": "boolean"
            },
            "welcomeToCourseAuthorization": {
              "type": "boolean"
            },
            "acceptTermsOfUse": {
              "type": "boolean"
            },
            "token": {
              "type": "string"
            }
          },
          "required": [
            "name",
            "email",
            "emailConfirm",
            "cpf",
            "gender",
            "mobileNumber",
            "password",
            "passwordConfirm",
            "regional",
            "unitId",
            "acceptTermsOfUse",
            "token"
          ]
        },
        "SubscribeStudentByInviteDto": {
          "type": "object",
          "properties": {
            "studentInviteToken": {
              "type": "string"
            },
            "courseId": {
              "type": "number"
            },
            "isPaid": {
              "type": "boolean"
            }
          },
          "required": [
            "studentInviteToken",
            "courseId",
            "isPaid"
          ]
        },
        "File": {
          "type": "object",
          "properties": {}
        },
        "student_commemorative_badges": {
          "type": "object",
          "properties": {
            "id": {
              "type": "number"
            },
            "studentId": {
              "type": "number"
            },
            "commemorativeBadgeId": {
              "type": "number"
            },
            "acquired": {
              "type": "boolean"
            },
            "createdAt": {
              "type": "string"
            },
            "commemorativeBadge": {
              "type": "object"
            }
          },
          "required": [
            "id",
            "studentId",
            "commemorativeBadgeId",
            "acquired",
            "createdAt",
            "commemorativeBadge"
          ]
        },
        "commemorative_badges": {
          "type": "object",
          "properties": {
            "id": {
              "type": "number"
            },
            "fileId": {
              "type": "number"
            },
            "title": {
              "type": "string"
            },
            "description": {
              "type": "string"
            },
            "startDate": {
              "format": "date-time",
              "type": "string"
            },
            "endDate": {
              "format": "date-time",
              "type": "string"
            },
            "available": {
              "type": "boolean"
            },
            "file": {
              "$ref": "#/components/schemas/File"
            },
            "studentCommemorativeBadges": {
              "$ref": "#/components/schemas/student_commemorative_badges"
            }
          },
          "required": [
            "id",
            "fileId",
            "title",
            "description",
            "startDate",
            "endDate",
            "available",
            "file",
            "studentCommemorativeBadges"
          ]
        },
        "CreateStudentCommemorativeBadgeDto": {
          "type": "object",
          "properties": {
            "studentId": {
              "type": "number"
            },
            "commemorativeBadgeId": {
              "type": "number"
            },
            "acquired": {
              "type": "boolean"
            }
          },
          "required": [
            "studentId",
            "commemorativeBadgeId",
            "acquired"
          ]
        },
        "UpdateStudentCommemorativeBadgeDto": {
          "type": "object",
          "properties": {
            "studentId": {
              "type": "number"
            },
            "commemorativeBadgeId": {
              "type": "number"
            },
            "acquired": {
              "type": "boolean"
            }
          }
        },
        "CommemorativeBadgeResponse": {
          "type": "object",
          "properties": {
            "studentCommemorativeBadge": {
              "$ref": "#/components/schemas/student_commemorative_badges"
            },
            "message": {
              "type": "string"
            }
          },
          "required": [
            "studentCommemorativeBadge",
            "message"
          ]
        },
        "covers": {
          "type": "object",
          "properties": {
            "id": {
              "type": "number"
            },
            "fileId": {
              "type": "number"
            },
            "title": {
              "type": "string"
            },
            "active": {
              "type": "boolean"
            },
            "file": {
              "$ref": "#/components/schemas/files"
            },
            "url": {
              "type": "string"
            },
            "createdAt": {
              "type": "string"
            },
            "updatedAt": {
              "type": "string"
            }
          },
          "required": [
            "id",
            "fileId",
            "title",
            "active",
            "file",
            "url",
            "createdAt",
            "updatedAt"
          ]
        },
        "colors_theme": {
          "type": "object",
          "properties": {
            "id": {
              "type": "number"
            },
            "backgroundColor": {
              "type": "string"
            },
            "primaryButtonColor": {
              "type": "string"
            },
            "secondaryButtonColor": {
              "type": "string"
            },
            "tertiaryButtonColor": {
              "type": "string"
            },
            "levelColor": {
              "type": "string"
            },
            "tagColor": {
              "type": "string"
            },
            "starColor": {
              "type": "string"
            },
            "primaryIconColor": {
              "type": "string"
            },
            "secondaryIconColor": {
              "type": "string"
            },
            "primaryProgressBarColor": {
              "type": "string"
            },
            "secondaryProgressBarColor": {
              "type": "string"
            },
            "tertiaryProgressBarColor": {
              "type": "string"
            },
            "lifeGradientBeginColor": {
              "type": "string"
            },
            "lifeGradientEndColor": {
              "type": "string"
            }
          },
          "required": [
            "id",
            "backgroundColor",
            "primaryButtonColor",
            "secondaryButtonColor",
            "tertiaryButtonColor",
            "levelColor",
            "tagColor",
            "starColor",
            "primaryIconColor",
            "secondaryIconColor",
            "primaryProgressBarColor",
            "secondaryProgressBarColor",
            "tertiaryProgressBarColor",
            "lifeGradientBeginColor",
            "lifeGradientEndColor"
          ]
        },
        "banner_theme": {
          "type": "object",
          "properties": {
            "id": {
              "type": "number"
            },
            "homeBackgroundId": {
              "type": "number"
            },
            "homeBackground": {
              "$ref": "#/components/schemas/files"
            },
            "playPlusId": {
              "type": "number"
            },
            "playPlus": {
              "$ref": "#/components/schemas/files"
            },
            "playWhatsId": {
              "type": "number"
            },
            "playWhats": {
              "$ref": "#/components/schemas/files"
            },
            "playListId": {
              "type": "number"
            },
            "playList": {
              "$ref": "#/components/schemas/files"
            }
          },
          "required": [
            "id",
            "homeBackgroundId",
            "homeBackground",
            "playPlusId",
            "playPlus",
            "playWhatsId",
            "playWhats",
            "playListId",
            "playList"
          ]
        },
        "themes": {
          "type": "object",
          "properties": {
            "id": {
              "type": "number"
            },
            "colorsThemeId": {
              "type": "number"
            },
            "colorsTheme": {
              "$ref": "#/components/schemas/colors_theme"
            },
            "bannerThemeId": {
              "type": "number"
            },
            "bannerTheme": {
              "$ref": "#/components/schemas/banner_theme"
            }
          },
          "required": [
            "id",
            "colorsThemeId",
            "colorsTheme",
            "bannerThemeId",
            "bannerTheme"
          ]
        },
        "companies": {
          "type": "object",
          "properties": {
            "id": {
              "type": "number"
            },
            "corporateName": {
              "type": "string"
            },
            "fantasyName": {
              "type": "string"
            },
            "cnpj": {
              "type": "string"
            },
            "ie": {
              "type": "string"
            },
            "address": {
              "type": "string"
            },
            "city": {
              "type": "string"
            },
            "state": {
              "type": "string"
            },
            "webAddress": {
              "type": "string"
            },
            "employeeNumber": {
              "type": "number"
            },
            "playSubdomain": {
              "type": "string"
            },
            "email": {
              "type": "string"
            },
            "acceptTermsOfUse": {
              "type": "boolean"
            },
            "isClient": {
              "type": "boolean"
            },
            "isPartner": {
              "type": "boolean"
            },
            "expiresAt": {
              "format": "date-time",
              "type": "string"
            },
            "logoFileId": {
              "type": "number"
            },
            "logo": {
              "$ref": "#/components/schemas/files"
            },
            "themes": {
              "$ref": "#/components/schemas/themes"
            }
          },
          "required": [
            "id",
            "corporateName",
            "fantasyName",
            "cnpj",
            "ie",
            "address",
            "city",
            "state",
            "webAddress",
            "employeeNumber",
            "playSubdomain",
            "email",
            "acceptTermsOfUse",
            "isClient",
            "isPartner",
            "expiresAt",
            "logoFileId",
            "logo",
            "themes"
          ]
        }
      }
    },
    "paths": {
      "/student-api/auth/login": {
        "post": {
          "operationId": "AuthController_login",
          "parameters": [],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/LoginDto"
                }
              }
            }
          },
          "responses": {
            "201": {
              "description": "The user has been successfully authorized."
            }
          },
          "tags": [
            "auth"
          ]
        }
      },
      "/student-api/auth/valid-token/{token}": {
        "get": {
          "operationId": "AuthController_validToken",
          "parameters": [
            {
              "name": "token",
              "required": true,
              "in": "path",
              "schema": {
                "type": "string"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Validate token and return Student data.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/students"
                  }
                }
              }
            }
          },
          "tags": [
            "auth"
          ]
        }
      },
      "/student-api/students": {
        "post": {
          "operationId": "StudentsController_create",
          "parameters": [],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/CreateStudentDto"
                }
              }
            }
          },
          "responses": {
            "201": {
              "description": "Student create sucessfully",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/StudentResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "students"
          ]
        },
        "put": {
          "operationId": "StudentsController_update",
          "parameters": [],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/UpdateStudentDTO"
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Student updated successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/StudentResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "students"
          ]
        }
      },
      "/student-api/students/photo": {
        "post": {
          "operationId": "StudentsController_uploadPhoto",
          "parameters": [],
          "responses": {
            "201": {
              "description": "Photo uploaded sucessfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/files"
                  }
                }
              }
            }
          },
          "tags": [
            "students"
          ]
        }
      },
      "/student-api/students/update-photo": {
        "put": {
          "operationId": "StudentsController_updatePhoto",
          "parameters": [],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/UpdatePhotoDto"
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Photo updated successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/students"
                  }
                }
              }
            }
          },
          "tags": [
            "students"
          ]
        }
      },
      "/student-api/students/password-recovery": {
        "post": {
          "operationId": "StudentsController_passwordRecovery",
          "parameters": [],
          "responses": {
            "200": {
              "description": "Passowrd recovered successfully."
            }
          },
          "tags": [
            "students"
          ]
        }
      },
      "/student-api/students/update-password": {
        "put": {
          "operationId": "StudentsController_token",
          "parameters": [
            {
              "name": "token",
              "required": true,
              "in": "query",
              "schema": {
                "type": "string"
              }
            }
          ],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/PasswordRecoveryDto"
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Passoword updated successfully."
            }
          },
          "tags": [
            "students"
          ]
        }
      },
      "/student-api/students/delete-account": {
        "post": {
          "operationId": "StudentsController_deleteAccount",
          "parameters": [],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/DeleteAccountDto"
                }
              }
            }
          },
          "responses": {
            "201": {
              "description": "Deleted account successfully."
            }
          },
          "tags": [
            "students"
          ]
        }
      },
      "/student-api/students/verify-hashname-to-email": {
        "post": {
          "operationId": "StudentsController_verifyHashnameToMail",
          "parameters": [],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/VerifyHashnameDto"
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Verified hashname to email successfully."
            }
          },
          "tags": [
            "students"
          ]
        }
      },
      "/student-api/students/create-email-unauthorization": {
        "post": {
          "operationId": "StudentsController_createEmailUnauthorizations",
          "parameters": [],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/CreateUnauthorizationDto"
                }
              }
            }
          },
          "responses": {
            "201": {
              "description": "Create email unauthorization successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "type": "boolean"
                  }
                }
              }
            }
          },
          "tags": [
            "students"
          ]
        }
      },
      "/student-api/students/currentDate": {
        "get": {
          "operationId": "StudentsController_getCurrentDate",
          "parameters": [],
          "responses": {
            "200": {
              "description": "Get current date successfully."
            }
          },
          "tags": [
            "students"
          ]
        }
      },
      "/student-api/subscriptions": {
        "post": {
          "operationId": "SubscriptionsController_create",
          "parameters": [],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/CreateSubscriptionDto"
                }
              }
            }
          },
          "responses": {
            "201": {
              "description": "Created subscription successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/SubscriptionControllerResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "subscriptions"
          ]
        }
      },
      "/student-api/subscriptions/verifyHashname": {
        "post": {
          "operationId": "SubscriptionsController_verifyIfHashnameExists",
          "parameters": [],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/VerifyIfHashnameExistsDto"
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Verified Hashname successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "type": "boolean"
                  }
                }
              }
            }
          },
          "tags": [
            "subscriptions"
          ]
        }
      },
      "/student-api/subscriptions/{id}": {
        "put": {
          "operationId": "SubscriptionsController_update",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/UpdateSubscriptionDto"
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Subscription updated successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/MessageResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "subscriptions"
          ]
        }
      },
      "/student-api/subscriptions/{subscriptionId}/progress": {
        "get": {
          "operationId": "SubscriptionsController_fetchProgress",
          "parameters": [
            {
              "name": "subscriptionId",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Get subscription progress successfully."
            }
          },
          "tags": [
            "subscriptions"
          ]
        }
      },
      "/student-api/subscriptions/{id}/evaluate-course": {
        "put": {
          "operationId": "SubscriptionsController_evaluateCourse",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/RatingCourseDto"
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Updated evaluation course successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/MessageResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "subscriptions"
          ]
        }
      },
      "/student-api/courses/{id}": {
        "get": {
          "operationId": "CoursesController_findCourseContent",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Get course content successfully."
            }
          },
          "tags": [
            "courses"
          ]
        }
      },
      "/student-api/courses/{id}/isPaid": {
        "get": {
          "operationId": "CoursesController_verifyIfIsPaid",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            },
            {
              "name": "userId",
              "required": true,
              "in": "query",
              "schema": {
                "type": "number"
              }
            },
            {
              "name": "sku",
              "required": true,
              "in": "query",
              "schema": {
                "type": "string"
              }
            },
            {
              "name": "affiliateCode",
              "required": true,
              "in": "query",
              "schema": {
                "type": "number"
              }
            },
            {
              "name": "branchCode",
              "required": true,
              "in": "query",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Verified if is paid successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "type": "boolean"
                  }
                }
              }
            }
          },
          "tags": [
            "courses"
          ]
        }
      },
      "/student-api/wishlist": {
        "post": {
          "operationId": "WishlistController_create",
          "parameters": [],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/CreateWishlistDto"
                }
              }
            }
          },
          "responses": {
            "201": {
              "description": "Created wishlist successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/MessageResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "wishlist"
          ]
        }
      },
      "/student-api/wishlist/{id}": {
        "delete": {
          "operationId": "WishlistController_remove",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Deleted wish successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/MessageResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "wishlist"
          ]
        }
      },
      "/student-api/activities": {
        "post": {
          "operationId": "ActivitiesController_create",
          "parameters": [],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/CreateActivityDto"
                }
              }
            }
          },
          "responses": {
            "201": {
              "description": "Created a new activity successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/activities"
                  }
                }
              }
            }
          },
          "tags": [
            "activities"
          ]
        }
      },
      "/student-api/activities/{id}": {
        "delete": {
          "operationId": "ActivitiesController_remove",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Deleted a activity successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/MessageResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "activities"
          ]
        },
        "get": {
          "operationId": "ActivitiesController_findOne",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Get activity successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/activities"
                  }
                }
              }
            }
          },
          "tags": [
            "activities"
          ]
        },
        "put": {
          "operationId": "ActivitiesController_update",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/CreateActivityDto"
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Updated activity successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/activities"
                  }
                }
              }
            }
          },
          "tags": [
            "activities"
          ]
        }
      },
      "/student-api/activities/answer": {
        "post": {
          "operationId": "ActivitiesController_createAnswer",
          "parameters": [],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/CreateAnswerDto"
                }
              }
            }
          },
          "responses": {
            "201": {
              "description": "Created a new answer successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CreateAnswerResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "activities"
          ]
        }
      },
      "/student-api/questions": {
        "post": {
          "operationId": "QuestionsController_create",
          "parameters": [],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/CreateQuestionsDto"
                }
              }
            }
          },
          "responses": {
            "201": {
              "description": "Question created successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/questions"
                  }
                }
              }
            }
          },
          "tags": [
            "questions"
          ]
        }
      },
      "/student-api/questions/{id}": {
        "delete": {
          "operationId": "QuestionsController_remove",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Question deleted successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/MessageResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "questions"
          ]
        },
        "put": {
          "operationId": "QuestionsController_update",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/CreateQuestionsDto"
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Question updated successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/questions"
                  }
                }
              }
            }
          },
          "tags": [
            "questions"
          ]
        }
      },
      "/student-api/alternatives": {
        "post": {
          "operationId": "AlternativesController_create",
          "parameters": [],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/CreateAlternativeDto"
                }
              }
            }
          },
          "responses": {
            "201": {
              "description": "Alternative created successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/alternatives"
                  }
                }
              }
            }
          },
          "tags": [
            "alternatives"
          ]
        }
      },
      "/student-api/alternatives/{id}": {
        "delete": {
          "operationId": "AlternativesController_remove",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Alternative deleted successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/MessageResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "alternatives"
          ]
        },
        "put": {
          "operationId": "AlternativesController_update",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/CreateAlternativeDto"
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Alternative updated successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/alternatives"
                  }
                }
              }
            }
          },
          "tags": [
            "alternatives"
          ]
        }
      },
      "/student-api/units": {
        "get": {
          "operationId": "UnitsController_findAll",
          "parameters": [],
          "responses": {
            "200": {
              "description": "Get units successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "type": "array",
                    "items": {
                      "$ref": "#/components/schemas/units"
                    }
                  }
                }
              }
            }
          },
          "tags": [
            "units"
          ]
        }
      },
      "/student-api/files": {
        "post": {
          "operationId": "FilesController_create",
          "parameters": [
            {
              "name": "type",
              "required": true,
              "in": "query",
              "schema": {
                "enum": [
                  "video",
                  "authorization_image_and_voice_term",
                  "copyright_assignment_term",
                  "attachment",
                  "image"
                ],
                "type": "string"
              }
            }
          ],
          "requestBody": {
            "required": true,
            "content": {
              "multipart/form-data": {
                "schema": {
                  "$ref": "#/components/schemas/FileUploadDto"
                }
              }
            }
          },
          "responses": {
            "201": {
              "description": "File successfully uploaded.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/files"
                  }
                }
              }
            }
          },
          "tags": [
            "files"
          ]
        }
      },
      "/student-api/interestAreas": {
        "post": {
          "operationId": "InterestAreasController_create",
          "parameters": [],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/CreateInterestAreaDto"
                }
              }
            }
          },
          "responses": {
            "201": {
              "description": "Created a new interest area successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "type": "array",
                    "items": {
                      "$ref": "#/components/schemas/MessageResponse"
                    }
                  }
                }
              }
            }
          },
          "tags": [
            "interestAreas"
          ]
        },
        "put": {
          "operationId": "InterestAreasController_update",
          "parameters": [],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/CreateInterestAreaDto"
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Interest area successfully uploaded.",
              "content": {
                "application/json": {
                  "schema": {
                    "type": "array",
                    "items": {
                      "$ref": "#/components/schemas/MessageResponse"
                    }
                  }
                }
              }
            }
          },
          "tags": [
            "interestAreas"
          ]
        },
        "get": {
          "operationId": "InterestAreasController_findAllAreasOfStudent",
          "parameters": [],
          "responses": {
            "200": {
              "description": "Get interests areas uploaded.",
              "content": {
                "application/json": {
                  "schema": {
                    "type": "array",
                    "items": {
                      "$ref": "#/components/schemas/interest_areas"
                    }
                  }
                }
              }
            }
          },
          "tags": [
            "interestAreas"
          ]
        }
      },
      "/student-api/subscription-tracking": {
        "post": {
          "operationId": "SubscriptionTrackingController_create",
          "parameters": [],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/CreateSubscriptionTrackingDto"
                }
              }
            }
          },
          "responses": {
            "201": {
              "description": "Create a subscription tracking sucessfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CreateSubscriptionTrackingResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "subscription-tracking"
          ]
        }
      },
      "/student-api/subscription-tracking/subscriptions/{subscriptionId}": {
        "get": {
          "operationId": "SubscriptionTrackingController_getCompletedActivitiesAndVideosBySubscription",
          "parameters": [
            {
              "name": "subscriptionId",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Get subscription sucessfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/GetSubscriptionTrackingResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "subscription-tracking"
          ]
        }
      },
      "/student-api/notifications/me": {
        "get": {
          "operationId": "NotificationsController_getNofications",
          "parameters": [],
          "responses": {
            "200": {
              "description": "Get notifications sucessfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/NotificationResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "notifications"
          ]
        }
      },
      "/student-api/notifications": {
        "put": {
          "operationId": "NotificationsController_updateNotification",
          "parameters": [],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/UpdateNotificationDto"
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Updated notifications sucessfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/MessageResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "notifications"
          ]
        }
      },
      "/student-api/certified": {
        "get": {
          "operationId": "CertifiedController_myCertificates",
          "parameters": [],
          "responses": {
            "200": {
              "description": "Get certificates sucessfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "type": "array",
                    "items": {
                      "$ref": "#/components/schemas/subscriptions"
                    }
                  }
                }
              }
            }
          },
          "tags": [
            "certified"
          ]
        },
        "post": {
          "operationId": "CertifiedController_create",
          "parameters": [],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/CreateCertifiedDto"
                }
              }
            }
          },
          "responses": {
            "201": {
              "description": "Created certificate sucessfully."
            }
          },
          "tags": [
            "certified"
          ]
        }
      },
      "/student-api/contact-us-submission": {
        "post": {
          "operationId": "ContactUsMailerController_sendEmail",
          "parameters": [],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/SendEmailDto"
                }
              }
            }
          },
          "responses": {
            "201": {
              "description": "Created contact us successfully."
            }
          },
          "tags": [
            "contact-us-submission"
          ]
        }
      },
      "/student-api/lessons": {
        "post": {
          "operationId": "LessonsController_create",
          "parameters": [],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/LessonDto"
                }
              }
            }
          },
          "responses": {
            "201": {
              "description": "Created study plan successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/LessonResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "lessons"
          ]
        }
      },
      "/student-api/lessons/{id}": {
        "put": {
          "operationId": "LessonsController_update",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/LessonDto"
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Updated lesson successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/LessonResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "lessons"
          ]
        },
        "delete": {
          "operationId": "LessonsController_delete",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Deleted lesson successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/MessageResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "lessons"
          ]
        }
      },
      "/student-api/lessons/videos": {
        "post": {
          "operationId": "LessonsController_addVideo",
          "parameters": [],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/LessonVideoDto"
                }
              }
            }
          },
          "responses": {
            "201": {
              "description": "Added video to lesson successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/LessonVideoResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "lessons"
          ]
        }
      },
      "/student-api/lessons/videos/{lessonVideoId}": {
        "delete": {
          "operationId": "LessonsController_deleteVideo",
          "parameters": [
            {
              "name": "lessonVideoId",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Created study plan successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/MessageResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "lessons"
          ]
        }
      },
      "/student-api/lessons/videos/activities": {
        "post": {
          "operationId": "LessonsController_addVideoActivity",
          "parameters": [],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/LessonVideoActivityDto"
                }
              }
            }
          },
          "responses": {
            "201": {
              "description": "Added video activity to lesson successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/LessonVideoAcitivtyResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "lessons"
          ]
        }
      },
      "/student-api/lessons/videos/activities/{lessonVideoActivityId}": {
        "delete": {
          "operationId": "LessonsController_deleteVideoActivity",
          "parameters": [
            {
              "name": "lessonVideoActivityId",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Created study plan successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/MessageResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "lessons"
          ]
        }
      },
      "/student-api/lessons/activities": {
        "post": {
          "operationId": "LessonsController_addActivity",
          "parameters": [],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/LessonActivityDto"
                }
              }
            }
          },
          "responses": {
            "201": {
              "description": "Add acitivity to lesson successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/LessonAcitivtyResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "lessons"
          ]
        }
      },
      "/student-api/lessons/activities/{lessonActivityId}": {
        "delete": {
          "operationId": "LessonsController_deleteActivity",
          "parameters": [
            {
              "name": "lessonActivityId",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Deleted acitivity of lesson successfully."
            }
          },
          "tags": [
            "lessons"
          ]
        }
      },
      "/student-api/lessons/attachments": {
        "post": {
          "operationId": "LessonsController_addAttachment",
          "parameters": [],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/LessonAttachmentDto"
                }
              }
            }
          },
          "responses": {
            "201": {
              "description": "Created lesson attachments successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/LessonAttachmentResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "lessons"
          ]
        }
      },
      "/student-api/lessons/attachments/{lessonAttachmentId}": {
        "delete": {
          "operationId": "LessonsController_deleteAttachment",
          "parameters": [
            {
              "name": "lessonAttachmentId",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Deleted lesson attachments successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/MessageResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "lessons"
          ]
        }
      },
      "/student-api/lessons/interactions": {
        "get": {
          "operationId": "LessonsController_getAllInteractions",
          "parameters": [],
          "responses": {
            "200": {
              "description": "Get all lesson interactions successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "type": "array",
                    "items": {
                      "$ref": "#/components/schemas/interactions"
                    }
                  }
                }
              }
            }
          },
          "tags": [
            "lessons"
          ]
        }
      },
      "/student-api/lessons/interactions/{lessonId}": {
        "get": {
          "operationId": "LessonsController_getInteractionsByLesson",
          "parameters": [
            {
              "name": "lessonId",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Get lessons interactions by lesson id successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "type": "array",
                    "items": {
                      "$ref": "#/components/schemas/interactions"
                    }
                  }
                }
              }
            }
          },
          "tags": [
            "lessons"
          ]
        }
      },
      "/student-api/lessons/interactions/positive/{lessonId}": {
        "get": {
          "operationId": "LessonsController_getPositiveInteractionsByLesson",
          "parameters": [
            {
              "name": "lessonId",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Get positive interactions by lesson id successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "type": "array",
                    "items": {
                      "$ref": "#/components/schemas/interactions"
                    }
                  }
                }
              }
            }
          },
          "tags": [
            "lessons"
          ]
        },
        "post": {
          "operationId": "LessonsController_sendPositiveInteractionsByLesson",
          "parameters": [
            {
              "name": "lessonId",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "201": {
              "description": "Send positive lesson interaction successfully."
            }
          },
          "tags": [
            "lessons"
          ]
        },
        "delete": {
          "operationId": "LessonsController_destroyPositiveInteraction",
          "parameters": [
            {
              "name": "lessonId",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "201": {
              "description": "Delete positive lesson interaction successfully."
            }
          },
          "tags": [
            "lessons"
          ]
        }
      },
      "/student-api/lessons/interactions/negative/{lessonId}": {
        "get": {
          "operationId": "LessonsController_getNegativeInteractionsByLesson",
          "parameters": [
            {
              "name": "lessonId",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Get negatives lessons interactions by lesson id successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "type": "array",
                    "items": {
                      "$ref": "#/components/schemas/interactions"
                    }
                  }
                }
              }
            }
          },
          "tags": [
            "lessons"
          ]
        },
        "post": {
          "operationId": "LessonsController_sendNegativeInteractionsByLesson",
          "parameters": [
            {
              "name": "lessonId",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "201": {
              "description": "Send negative lesson interaction successfully."
            }
          },
          "tags": [
            "lessons"
          ]
        },
        "delete": {
          "operationId": "LessonsController_destroyNegativeInteraction",
          "parameters": [
            {
              "name": "lessonId",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "201": {
              "description": "Delete negative lesson interaction successfully."
            }
          },
          "tags": [
            "lessons"
          ]
        }
      },
      "/student-api/lessons/interactions/myInteraction/{lessonId}": {
        "get": {
          "operationId": "LessonsController_getUserReactionInLesson",
          "parameters": [
            {
              "name": "lessonId",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Get all user interactions by lesson id successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "type": "array",
                    "items": {
                      "$ref": "#/components/schemas/interactions"
                    }
                  }
                }
              }
            }
          },
          "tags": [
            "lessons"
          ]
        }
      },
      "/student-api/study-plan": {
        "post": {
          "operationId": "StudyPlanController_create",
          "parameters": [],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/StudyPlanDTO"
                }
              }
            }
          },
          "responses": {
            "201": {
              "description": "Created study plan successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/study_plans"
                  }
                }
              }
            }
          },
          "tags": [
            "study-plan"
          ]
        }
      },
      "/student-api/study-plan/{id}": {
        "put": {
          "operationId": "StudyPlanController_update",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/StudyPlanDTO"
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Updated study plan successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/MessageResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "study-plan"
          ]
        },
        "delete": {
          "operationId": "StudyPlanController_delete",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Deleted study plan successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/MessageResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "study-plan"
          ]
        }
      },
      "/student-api/study-plan/{id}/follow": {
        "post": {
          "operationId": "StudyPlanController_follow",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "201": {
              "description": "Follow study plan successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/MessageResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "study-plan"
          ]
        }
      },
      "/student-api/study-plan/{id}/unfollow": {
        "delete": {
          "operationId": "StudyPlanController_unfollow",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Unfollow study plan successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/MessageResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "study-plan"
          ]
        }
      },
      "/student-api/study-plan/add-course": {
        "post": {
          "operationId": "StudyPlanController_addCourse",
          "parameters": [],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/StudyPlanCourseDTO"
                }
              }
            }
          },
          "responses": {
            "201": {
              "description": "Add course to study plan successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/MessageResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "study-plan"
          ]
        }
      },
      "/student-api/study-plan/remove-course": {
        "post": {
          "operationId": "StudyPlanController_removeCourse",
          "parameters": [],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/StudyPlanCourseDTO"
                }
              }
            }
          },
          "responses": {
            "201": {
              "description": "Deleted course of study plan successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/MessageResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "study-plan"
          ]
        }
      },
      "/student-api/study-plan/{id}/badge": {
        "get": {
          "operationId": "StudyPlanController_emitBadge",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Emit study plan badge successfully."
            }
          },
          "tags": [
            "study-plan"
          ]
        }
      },
      "/student-api/study-plan/badges": {
        "get": {
          "operationId": "StudyPlanController_getStudentBadges",
          "parameters": [],
          "responses": {
            "200": {
              "description": "Get emitted study plan badges successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/EmitedBadgeResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "study-plan"
          ]
        }
      },
      "/student-api/student-actions-multiplier": {
        "get": {
          "operationId": "StudentActionsMultiplierController_findAll",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Get all student actions multiplier successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "type": "array",
                    "items": {
                      "$ref": "#/components/schemas/student_action_multipliers"
                    }
                  }
                }
              }
            }
          },
          "tags": [
            "student_actions_multiplier"
          ]
        },
        "post": {
          "operationId": "StudentActionsMultiplierController_create",
          "parameters": [],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/StudentActionMultiplierDto"
                }
              }
            }
          },
          "responses": {
            "201": {
              "description": "Created student action multiplier successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/student_action_multipliers"
                  }
                }
              }
            }
          },
          "tags": [
            "student_actions_multiplier"
          ]
        }
      },
      "/student-api/student-actions-multiplier/{id}": {
        "get": {
          "operationId": "StudentActionsMultiplierController_findOne",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Get student action multiplier successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/student_action_multipliers"
                  }
                }
              }
            }
          },
          "tags": [
            "student_actions_multiplier"
          ]
        },
        "put": {
          "operationId": "StudentActionsMultiplierController_update",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/StudentActionMultiplierDto"
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Updated student action multiplier successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/student_action_multipliers"
                  }
                }
              }
            }
          },
          "tags": [
            "student_actions_multiplier"
          ]
        },
        "delete": {
          "operationId": "StudentActionsMultiplierController_remove",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "201": {
              "description": "Deleted student action multiplier successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/MessageResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "student_actions_multiplier"
          ]
        }
      },
      "/student-api/student-points": {
        "get": {
          "operationId": "StudentPointsController_findAll",
          "parameters": [],
          "responses": {
            "200": {
              "description": "Get all student points successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "type": "array",
                    "items": {
                      "$ref": "#/components/schemas/student_points"
                    }
                  }
                }
              }
            }
          },
          "tags": [
            "student_points"
          ]
        },
        "post": {
          "operationId": "StudentPointsController_create",
          "parameters": [],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/StudentPointDto"
                }
              }
            }
          },
          "responses": {
            "201": {
              "description": "Created student points successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/student_points"
                  }
                }
              }
            }
          },
          "tags": [
            "student_points"
          ]
        }
      },
      "/student-api/student-points/{id}": {
        "get": {
          "operationId": "StudentPointsController_findOne",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Get student points successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/student_points"
                  }
                }
              }
            }
          },
          "tags": [
            "student_points"
          ]
        },
        "put": {
          "operationId": "StudentPointsController_update",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/StudentPointDto"
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Updated student points successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/student_points"
                  }
                }
              }
            }
          },
          "tags": [
            "student_points"
          ]
        },
        "delete": {
          "operationId": "StudentPointsController_remove",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Deleted student points successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/MessageResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "student_points"
          ]
        }
      },
      "/student-api/news": {
        "get": {
          "operationId": "NewsController_findAll",
          "parameters": [],
          "responses": {
            "200": {
              "description": "Get news successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "type": "array",
                    "items": {
                      "$ref": "#/components/schemas/news"
                    }
                  }
                }
              }
            }
          },
          "tags": [
            "news"
          ]
        }
      },
      "/student-api/news/mostViewed": {
        "get": {
          "operationId": "NewsController_mostViewed",
          "parameters": [],
          "responses": {
            "200": {
              "description": "Get the most viewed news successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "type": "array",
                    "items": {
                      "$ref": "#/components/schemas/news"
                    }
                  }
                }
              }
            }
          },
          "tags": [
            "news"
          ]
        }
      },
      "/student-api/news/paged": {
        "get": {
          "operationId": "NewsController_findAllPaginated",
          "parameters": [],
          "responses": {
            "200": {
              "description": "Get news paginated successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "type": "array",
                    "items": {
                      "$ref": "#/components/schemas/news"
                    }
                  }
                }
              }
            }
          },
          "tags": [
            "news"
          ]
        }
      },
      "/student-api/news/{id}": {
        "get": {
          "operationId": "NewsController_findOne",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Get news by id successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/news"
                  }
                }
              }
            }
          },
          "tags": [
            "news"
          ]
        }
      },
      "/student-api/news/moreNews/{id}": {
        "get": {
          "operationId": "NewsController_findMoreNews",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Get more news successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "type": "array",
                    "items": {
                      "$ref": "#/components/schemas/news"
                    }
                  }
                }
              }
            }
          },
          "tags": [
            "news"
          ]
        }
      },
      "/student-api/news/recommended/{id}": {
        "get": {
          "operationId": "NewsController_recommendedNews",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Get recomemnded news successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "type": "array",
                    "items": {
                      "$ref": "#/components/schemas/news"
                    }
                  }
                }
              }
            }
          },
          "tags": [
            "news"
          ]
        }
      },
      "/student-api/surveys/active": {
        "get": {
          "operationId": "SurveyController_findActive",
          "parameters": [],
          "responses": {
            "200": {
              "description": "Get active survey successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/surveys"
                  }
                }
              }
            }
          },
          "tags": [
            "surveys"
          ]
        }
      },
      "/student-api/survey-answers/{subscriptionId}": {
        "get": {
          "operationId": "SurveyAnswersController_checkStudentStatus",
          "parameters": [
            {
              "name": "subscriptionId",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Check student status successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/StudentStatusResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "surveys"
          ]
        }
      },
      "/student-api/survey-answers": {
        "post": {
          "operationId": "SurveyAnswersController_createInBulk",
          "parameters": [],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/SurveyAnswerDto"
                  }
                }
              }
            }
          },
          "responses": {
            "201": {
              "description": "Created survey answers successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/survey_answer"
                  }
                }
              }
            }
          },
          "tags": [
            "surveys"
          ]
        }
      },
      "/student-api/columnists": {
        "get": {
          "operationId": "ColumnistsController_findAll",
          "parameters": [],
          "responses": {
            "200": {
              "description": "Get all columnists successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "type": "array",
                    "items": {
                      "$ref": "#/components/schemas/users"
                    }
                  }
                }
              }
            }
          },
          "tags": [
            "columnists"
          ]
        }
      },
      "/student-api/curriculum/work-experiences/{curriculumId}": {
        "get": {
          "operationId": "CurriculumWorkExperiencesController_findAll",
          "parameters": [
            {
              "name": "curriculumId",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Get all curriculum work experience successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "type": "array",
                    "items": {
                      "$ref": "#/components/schemas/curriculum_work_experiences"
                    }
                  }
                }
              }
            }
          },
          "tags": [
            "curriculum"
          ]
        }
      },
      "/student-api/curriculum/work-experiences/id/{id}": {
        "get": {
          "operationId": "CurriculumWorkExperiencesController_findOne",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Get curriculum work experience successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/curriculum_work_experiences"
                  }
                }
              }
            }
          },
          "tags": [
            "curriculum"
          ]
        }
      },
      "/student-api/curriculum/work-experiences": {
        "post": {
          "operationId": "CurriculumWorkExperiencesController_create",
          "parameters": [],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/CurriculumWorkExperienceDto"
                }
              }
            }
          },
          "responses": {
            "201": {
              "description": "Created curriculum work experience successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/curriculum_work_experiences"
                  }
                }
              }
            }
          },
          "tags": [
            "curriculum"
          ]
        }
      },
      "/student-api/curriculum/work-experiences/{id}": {
        "put": {
          "operationId": "CurriculumWorkExperiencesController_update",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/UpdateCurriculumWorkExperienceDto"
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Updated curriculum work experience successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/MessageResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "curriculum"
          ]
        },
        "delete": {
          "operationId": "CurriculumWorkExperiencesController_remove",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Deleted curriculum work experience successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/MessageResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "curriculum"
          ]
        }
      },
      "/student-api/curriculum/educational-backgrounds/{curriculumId}": {
        "get": {
          "operationId": "CurriculumEducationalBackgroundsController_findAll",
          "parameters": [
            {
              "name": "curriculumId",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Get all curriculum educational background successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "type": "array",
                    "items": {
                      "$ref": "#/components/schemas/curriculum_educational_background"
                    }
                  }
                }
              }
            }
          },
          "tags": [
            "curriculum"
          ]
        }
      },
      "/student-api/curriculum/educational-backgrounds/id/{id}": {
        "get": {
          "operationId": "CurriculumEducationalBackgroundsController_findOne",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Get curriculum educational background successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/curriculum_educational_background"
                  }
                }
              }
            }
          },
          "tags": [
            "curriculum"
          ]
        }
      },
      "/student-api/curriculum/educational-backgrounds": {
        "post": {
          "operationId": "CurriculumEducationalBackgroundsController_create",
          "parameters": [],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/CurriculumEducationalBackgroundDto"
                }
              }
            }
          },
          "responses": {
            "201": {
              "description": "Created curriculum educational background successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/curriculum_educational_background"
                  }
                }
              }
            }
          },
          "tags": [
            "curriculum"
          ]
        }
      },
      "/student-api/curriculum/educational-backgrounds/{id}": {
        "put": {
          "operationId": "CurriculumEducationalBackgroundsController_update",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/UpdateCurriculumEducationalBackgroundDto"
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Updated curriculum educational background successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/MessageResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "curriculum"
          ]
        },
        "delete": {
          "operationId": "CurriculumEducationalBackgroundsController_remove",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Deleted curriculum educational background successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/MessageResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "curriculum"
          ]
        }
      },
      "/student-api/curriculum/external-certificates/{curriculumId}": {
        "get": {
          "operationId": "CurriculumExternalCertificatesController_findAll",
          "parameters": [
            {
              "name": "curriculumId",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Get all curriculum external certificates successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "type": "array",
                    "items": {
                      "$ref": "#/components/schemas/curriculum_external_certificates"
                    }
                  }
                }
              }
            }
          },
          "tags": [
            "curriculum"
          ]
        }
      },
      "/student-api/curriculum/external-certificates/id/{id}": {
        "get": {
          "operationId": "CurriculumExternalCertificatesController_findOne",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Get curriculum external certificates successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/curriculum_external_certificates"
                  }
                }
              }
            }
          },
          "tags": [
            "curriculum"
          ]
        }
      },
      "/student-api/curriculum/external-certificates": {
        "post": {
          "operationId": "CurriculumExternalCertificatesController_create",
          "parameters": [],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/CurriculumExternalCertificateDto"
                }
              }
            }
          },
          "responses": {
            "201": {
              "description": "Created curriculum external certificates successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/curriculum_external_certificates"
                  }
                }
              }
            }
          },
          "tags": [
            "curriculum"
          ]
        }
      },
      "/student-api/curriculum/external-certificates/{id}": {
        "put": {
          "operationId": "CurriculumExternalCertificatesController_update",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/UpdateCurriculumExternalCertificateDto"
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Updated curriculum external certificates successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/MessageResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "curriculum"
          ]
        },
        "delete": {
          "operationId": "CurriculumExternalCertificatesController_remove",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Deleted curriculum external certificates successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/MessageResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "curriculum"
          ]
        }
      },
      "/student-api/curriculum/{studentId}": {
        "get": {
          "operationId": "CurriculumController_findOrCreate",
          "parameters": [
            {
              "name": "studentId",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Get or create curriculum successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/curriculum"
                  }
                }
              }
            }
          },
          "tags": [
            "curriculum"
          ]
        }
      },
      "/student-api/curriculum/shared/{curriculumId}": {
        "get": {
          "operationId": "CurriculumController_findByCurriculumId",
          "parameters": [
            {
              "name": "curriculumId",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Get curriculum by id successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/curriculum"
                  }
                }
              }
            }
          },
          "tags": [
            "curriculum"
          ]
        }
      },
      "/student-api/curriculum/{id}": {
        "put": {
          "operationId": "CurriculumController_update",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/UpdateCurriculumDto"
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Updated curriculum successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/MessageResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "curriculum"
          ]
        },
        "delete": {
          "operationId": "CurriculumController_remove",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Deleted curriculum successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/MessageResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "curriculum"
          ]
        }
      },
      "/student-api/curriculum/generate/pdf": {
        "post": {
          "operationId": "CurriculumController_generatePdfCurriculum",
          "parameters": [],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/GeneratePdfCurriculumDto"
                }
              }
            }
          },
          "responses": {
            "201": {
              "description": "Generated curriculum pdf successfully."
            }
          },
          "tags": [
            "curriculum"
          ]
        }
      },
      "/student-api/curriculum/languages/{curriculumId}": {
        "get": {
          "operationId": "CurriculumLanguagesController_findAll",
          "parameters": [
            {
              "name": "curriculumId",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Get all curriculum languages successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "type": "array",
                    "items": {
                      "$ref": "#/components/schemas/curriculum_languages"
                    }
                  }
                }
              }
            }
          },
          "tags": [
            "curriculum"
          ]
        }
      },
      "/student-api/curriculum/languages/id/{id}": {
        "get": {
          "operationId": "CurriculumLanguagesController_findOne",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Get curriculum languages successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/curriculum_languages"
                  }
                }
              }
            }
          },
          "tags": [
            "curriculum"
          ]
        }
      },
      "/student-api/curriculum/languages": {
        "post": {
          "operationId": "CurriculumLanguagesController_create",
          "parameters": [],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/CurriculumLanguageDto"
                }
              }
            }
          },
          "responses": {
            "201": {
              "description": "Created curriculum languages successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/curriculum_languages"
                  }
                }
              }
            }
          },
          "tags": [
            "curriculum"
          ]
        }
      },
      "/student-api/curriculum/languages/{id}": {
        "put": {
          "operationId": "CurriculumLanguagesController_update",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/UpdateCurriculumLanguageDto"
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Updated curriculum languages successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/MessageResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "curriculum"
          ]
        },
        "delete": {
          "operationId": "CurriculumLanguagesController_remove",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Deleted curriculum languages successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/MessageResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "curriculum"
          ]
        }
      },
      "/student-api/curriculum/social-medias/{curriculumId}": {
        "get": {
          "operationId": "CurriculumSocialMediasController_findAll",
          "parameters": [
            {
              "name": "curriculumId",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Get all curriculum social media successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "type": "array",
                    "items": {
                      "$ref": "#/components/schemas/curriculum_social_medias"
                    }
                  }
                }
              }
            }
          },
          "tags": [
            "curriculum"
          ]
        }
      },
      "/student-api/curriculum/social-medias/id/{id}": {
        "get": {
          "operationId": "CurriculumSocialMediasController_findOne",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Get curriculum social media successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/curriculum_social_medias"
                  }
                }
              }
            }
          },
          "tags": [
            "curriculum"
          ]
        }
      },
      "/student-api/curriculum/social-medias": {
        "post": {
          "operationId": "CurriculumSocialMediasController_create",
          "parameters": [],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/CurriculumSocialMediaDto"
                }
              }
            }
          },
          "responses": {
            "201": {
              "description": "Created curriculum social media successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/curriculum_social_medias"
                  }
                }
              }
            }
          },
          "tags": [
            "curriculum"
          ]
        }
      },
      "/student-api/curriculum/social-medias/{id}": {
        "put": {
          "operationId": "CurriculumSocialMediasController_update",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/UpdateCurriculumSocialMediaDto"
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Updated curriculum social media successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/MessageResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "curriculum"
          ]
        },
        "delete": {
          "operationId": "CurriculumSocialMediasController_remove",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Deleted curriculum social media successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/MessageResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "curriculum"
          ]
        }
      },
      "/student-api/veracity/{subscriptionId}": {
        "get": {
          "operationId": "VeracityController_findOne",
          "parameters": [
            {
              "name": "subscriptionId",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Check veracity successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/student-activity-veracities"
                  }
                }
              }
            }
          },
          "tags": [
            "veracity"
          ]
        }
      },
      "/student-api/veracity": {
        "post": {
          "operationId": "VeracityController_create",
          "parameters": [],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/VeracityDto"
                }
              }
            }
          },
          "responses": {
            "201": {
              "description": "Created veracity successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/student-activity-veracities"
                  }
                }
              }
            }
          },
          "tags": [
            "veracity"
          ]
        }
      },
      "/student-api/banners": {
        "get": {
          "operationId": "BannersController_getBanners",
          "parameters": [],
          "responses": {
            "200": {
              "description": "Get all banners successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/banners"
                  }
                }
              }
            }
          },
          "tags": [
            "banner"
          ]
        }
      },
      "/student-api/student-invites/check-token/{token}": {
        "get": {
          "operationId": "StudentInvitesController_checkToken",
          "parameters": [
            {
              "name": "token",
              "required": true,
              "in": "path",
              "schema": {
                "type": "string"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Check student invite token successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/student_invites"
                  }
                }
              }
            }
          },
          "tags": [
            "student-invites"
          ]
        }
      },
      "/student-api/student-invites": {
        "post": {
          "operationId": "StudentInvitesController_createByInvite",
          "parameters": [],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/InvitedStudentDto"
                }
              }
            }
          },
          "responses": {
            "201": {
              "description": "Created student invite successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/MessageResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "student-invites"
          ]
        }
      },
      "/student-api/student-invites/{studentId}": {
        "post": {
          "operationId": "StudentInvitesController_subscribeByInvite",
          "parameters": [
            {
              "name": "studentId",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/SubscribeStudentByInviteDto"
                  }
                }
              }
            }
          },
          "responses": {
            "201": {
              "description": "Created invite by subscribe successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/MessageResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "student-invites"
          ]
        }
      },
      "/student-api/commemorative-badges/available/{studentId}": {
        "get": {
          "operationId": "CommemorativeBadgesController_findAllAvailable",
          "parameters": [
            {
              "name": "studentId",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Get all available commemorative badges successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "type": "array",
                    "items": {
                      "$ref": "#/components/schemas/commemorative_badges"
                    }
                  }
                }
              }
            }
          },
          "tags": [
            "commemorative-badges"
          ]
        }
      },
      "/student-api/commemorative-badges/acquired/{studentId}": {
        "get": {
          "operationId": "CommemorativeBadgesController_findAllAcquired",
          "parameters": [
            {
              "name": "studentId",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Get all acquired commemorative badges successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "type": "array",
                    "items": {
                      "$ref": "#/components/schemas/commemorative_badges"
                    }
                  }
                }
              }
            }
          },
          "tags": [
            "commemorative-badges"
          ]
        }
      },
      "/student-api/commemorative-badges/{id}": {
        "get": {
          "operationId": "CommemorativeBadgesController_findOne",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Get commemorative badge successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/student_commemorative_badges"
                  }
                }
              }
            }
          },
          "tags": [
            "commemorative-badges"
          ]
        },
        "put": {
          "operationId": "CommemorativeBadgesController_update",
          "parameters": [
            {
              "name": "id",
              "required": true,
              "in": "path",
              "schema": {
                "type": "number"
              }
            }
          ],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/UpdateStudentCommemorativeBadgeDto"
                }
              }
            }
          },
          "responses": {
            "200": {
              "description": "Updated student commemorative badge successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/CommemorativeBadgeResponse"
                  }
                }
              }
            }
          },
          "tags": [
            "commemorative-badges"
          ]
        }
      },
      "/student-api/commemorative-badges": {
        "post": {
          "operationId": "CommemorativeBadgesController_create",
          "parameters": [],
          "requestBody": {
            "required": true,
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/CreateStudentCommemorativeBadgeDto"
                }
              }
            }
          },
          "responses": {
            "201": {
              "description": "Created student commemorative badge successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/student_commemorative_badges"
                  }
                }
              }
            }
          },
          "tags": [
            "commemorative-badges"
          ]
        }
      },
      "/student-api/cover": {
        "get": {
          "operationId": "HomepageCoverController_getActive",
          "parameters": [],
          "responses": {
            "200": {
              "description": "Get active cover successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/covers"
                  }
                }
              }
            }
          },
          "tags": [
            "cover"
          ]
        }
      },
      "/student-api/themes/{playSubdomain}": {
        "get": {
          "operationId": "ThemesController_findThemeByPlayDomain",
          "parameters": [
            {
              "name": "playSubdomain",
              "required": true,
              "in": "path",
              "schema": {
                "type": "string"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Get themes by play subdomain successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/companies"
                  }
                }
              }
            }
          },
          "tags": [
            "themes"
          ]
        }
      },
      "/student-api/themes/check-domain/{playSubdomain}": {
        "get": {
          "operationId": "ThemesController_checkPlayDomainExists",
          "parameters": [
            {
              "name": "playSubdomain",
              "required": true,
              "in": "path",
              "schema": {
                "type": "string"
              }
            }
          ],
          "responses": {
            "200": {
              "description": "Check play subdomain successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "type": "boolean"
                  }
                }
              }
            }
          },
          "tags": [
            "themes"
          ]
        }
      }
    }
  },
  "customOptions": {},
  "swaggerUrl": {}
};
  url = options.swaggerUrl || url
  var urls = options.swaggerUrls
  var customOptions = options.customOptions
  var spec1 = options.swaggerDoc
  var swaggerOptions = {
    spec: spec1,
    url: url,
    urls: urls,
    dom_id: '#swagger-ui',
    deepLinking: true,
    presets: [
      SwaggerUIBundle.presets.apis,
      SwaggerUIStandalonePreset
    ],
    plugins: [
      SwaggerUIBundle.plugins.DownloadUrl
    ],
    layout: "StandaloneLayout"
  }
  for (var attrname in customOptions) {
    swaggerOptions[attrname] = customOptions[attrname];
  }
  var ui = SwaggerUIBundle(swaggerOptions)

  if (customOptions.oauth) {
    ui.initOAuth(customOptions.oauth)
  }

  if (customOptions.authAction) {
    ui.authActions.authorize(customOptions.authAction)
  }

  window.ui = ui
}
